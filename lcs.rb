=begin
LCS Problem Statement: Given two sequences, find the length of longest subsequence present in both of them. A subsequence is a sequence that appears in the same relative order, but not necessarily contiguous. For example, “abc”, “abg”, “bdf”, “aeg”, ‘”acefg”, .. etc are subsequences of “abcdefg”. So a string of length n has 2^n different possible subsequences.

It is a classic computer science problem, the basis of diff (a file comparison program that outputs the differences between two files), and has applications in bioinformatics.

Examples:
LCS for input Sequences “ABCDGH” and “AEDFHR” is “ADH” of length 3.
LCS for input Sequences “AGGTAB” and “GXTXAYB” is “GTAB” of length 4.
=end

def lcs(s1,s2)
  # let s1[0...i] and s2[0...j]
  # Say we know L(s1[0...i-1], s2[0...j-1])
  # L()
  # L (s1[0...i],s2[0...j])
  #  if s1[i-1] == s2[j-1]
  #      L(s1[0...i],s2[0...j]) = L(s1[0...i-1], s2[0...j-1]) + 1
  #      i++; j ++
  #  elsif s1[i-1] == nil
  #      return L(s1[0...i-1], s2[0...j-1])
  # elsif s2[j-1] == nil
  #      return L(s1[0...i-1], s2[0...j-1])
  # else
  #      L(s1[0...i],s2[0...j]) = max(L(s1[0...i-1],s2[0...j]), L(s1[0...i],s2[0...j-1]))

  return 0 if (s1.empty? or s2.empty?)
  if s1[0] == s2[0]
    return 1 + lcs(s1[1...s1.size], s2[1...s2.size])
  else
    return max(lcs(s1[1...s1.size], s2[0...s2.size]), lcs(s1[0...s1.size], s2[1...s2.size]))
  end
end

def max(a,b)
  a > b ? a : b
end

puts lcs('abc', 'ac')

=begin
s1 = ABC
s2 = AC

L(2,2) = 1+L(1,1)
L(1,1) = max(L(1,0), L(0,1))
L(1,0) = L()
L(0,1) = 0

=end