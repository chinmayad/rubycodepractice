=begin
Given an array of meetings, find out the minimum number of conference rooms required.

class Meeting
{
long startTime;
long endTime;
};

=end


# meetings is an array of (arrays of size 2). Ex : [['1:30', '2:15'], ['7:00', '9:00'], ['15:00', '15:30'], ['23:30', '00:15']]
# assumptions :
class Scheduler
  def initialize
    @intervals = Array.new(24 * 4) {0}
    # interval is a 24 * 4 size array where index 0 corresponds to 0000 hrs, index 1 corresponds to 0015 hrs. Value
    # at index 0 corresponds to number of meetings scheduled at 0000 to 0015 15 min interval
  end

  def room_allocator(meetings)
    meetings.each do |m|
      stime = translate(m[0])
      etime = translate(m[1])
      if etime < stime # meeting goes over midnight
        etime = 2360 # anchor to present day bookings
      end
      raise 'meeting time exceeds 24 hours present in a day' if etime > 2400 || stime > 2400
      sindex = get_index(stime)
      eindex = get_index(etime - 15)
      set_interval(sindex,eindex)
    end
    max_intersect = 0
    @intervals.each do |int|
      if int > max_intersect
        max_intersect = int
      end
    end
    return max_intersect
  end

  def translate(time)
    hours = time.tr(':', '') # replace colon with nothing
    raise 'non digits found in meeting times' if not hours.match(/^\d+$/)  # raise exception if non digits found
    hours.to_i
  end

  def set_interval(s,e)
    while s <= e
      @intervals[s] += 1
      s += 1
    end
  end

  def get_index(slot)
    hours = slot / 100
    mins = slot % 100
    index = (hours * 4 + mins/15)
    raise if index > 95
    return index
  end
end

m = [['1:30', '8:45'], ['7:00', '9:00'], ['15:00', '15:30'], ['23:30', '00:15'], ['8:30', '15:15']]
s = Scheduler.new
puts s.room_allocator(m)