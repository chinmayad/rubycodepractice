=begin

Given an input sentence, reverse the word order.  This transformation should be performed in-place.

For instance:

“How are you.”

Would be transformed to be:

“.you are How”
=end

def word_reverse(str)
  str.reverse!
  begSpace = 0
  for i in 0..str.size do
    if str[i] == ' ' || i == str.size
      str[begSpace...i] = str[begSpace...i].reverse!
      begSpace = i+1
    end
  end
  str
end


puts word_reverse('hello world')
puts word_reverse('how are you.')
puts word_reverse('.')
puts word_reverse('you.')
puts word_reverse('how d')
puts word_reverse(' ')