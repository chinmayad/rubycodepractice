class Employee
  @@employees = []
  def initialize(name, des, id=nil)
    @name = name
    @designation = des
    if id == nil
      @id = genID
    else
      @id = id
    end
    @@employees.push(self)
  end

  def self.list_employees
    @@employees.dup
  end

  def genID
    rand(99999)
  end
  attr_accessor :name, :id, :designation
end

class Respondent < Employee
  @@respondents = []
  def initialize(name, id=nil)
    super(name, 'RESPONDENT', id)
    @status = 'free'
    @@respondents.push(self)
  end

  def self.list_respondents
    @@respondents.dup
  end

  def self.list_free_respondents
    @@respondents.dup
  end
end

class Manager < Employee
  @@managers = []
  def initialize(name, id=nil)
    super(name, 'MANAGER', id)
    @status = 'free'
    @@managers.push(self)
  end

  def self.list_managers
    @@managers.dup
  end

  def self.list_free_managers
    @@managers.dup
  end
end

class Director < Employee
  @@directors = []
  def initialize(name, id=nil)
    super(name, 'DIRECTOR', id)
    @status = 'free'
    @@directors.push(self)
  end

  def self.list_directors
    @@directors.dup
  end

  def self.list_free_directors
    @@directors.dup
  end
end

def dispatchCall
  reslist = Respondent.list_free_respondents
  return reslist.pop.name if reslist.size != 0

  manlist = Manager.list_free_managers
  return manlist.pop.name if manlist.size != 0

  dirlist = Director.list_free_directors
  return dirlist.pop.name if dirlist.size != 0
end


#r1 = Respondent.new('doctor', 103)
#r2 = Respondent.new('soum', 105)
#r3 = Respondent.new('chin', 107)

#m1 = Manager.new('dumma', 113)
#m2 = Manager.new('tisha', 115)
#m3 = Manager.new('priyanka', 117)

d1 = Director.new('kaushik', 123)
d2 = Director.new('geeku', 125)
d3 = Director.new('rahul', 127)

puts dispatchCall