=begin
 balance a binary tree
 https://stackoverflow.com/questions/4597650/code-with-explanation-for-binary-tree-rotation-left-or-right
=end

class Node
  def initialize(value, left=nil, right=nil)
    @value = value
    @left = left
    @right = right
  end
  attr_accessor :left, :right, :value
end

def balanceBST(root)
  return root, 0 if root == nil
  newRoot = root
  root.left, leftDepth  = balanceBST(root.left)
  root.right, rightDepth = balanceBST(root.right)

  if leftDepth - rightDepth > 1
    newRoot = rightRotateBTree(root)
    leftDepth -= 1
    rightDepth += 1
  end

  if rightDepth - leftDepth > 1
    newRoot = leftRotateBTree(root)
    leftDepth += 1
    rightDepth -= 1
  end
  maxDepth = 1 + max(leftDepth, rightDepth)
  return newRoot, maxDepth
end

def max(a,b)
  a > b ? a : b
end

def rightRotateBTree(root)
  return root if (root == nil || root.left == nil)
  newRoot = root.left
  root.left = newRoot.right
  newRoot.right = root
  newRoot
end

def leftRotateBTree(root)
  return root if (root == nil || root.right == nil)
  newRoot = root.right
  root.right = newRoot.left
  newRoot.left = root
  newRoot
end


mytree = Node.new(7, Node.new(8, Node.new(13), Node.new(44)), Node.new(15, Node.new(3), Node.new(14, Node.new(1),
                                                                                                 Node.new(17))));

mytree = Node.new(1, Node.new(2), Node.new(3, nil, Node.new(5, nil, Node.new(7))))
t = balanceBST(mytree)
puts 'hi'

=begin
              1
           2     3
                    5
                       7
=end
