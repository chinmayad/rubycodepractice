# given n = 3, generate all valid combs of paranthesis.
# ans = ['((()))', '()()()', '(()())', '(())()', '()(())']


def gen_p(n)
  return [] if n == 0
  result = []
  insert_p(result,'', n, 0, 0)
  result
end

def counter
  @var = 0 unless @var
  @var += 1
end

def insert_p(res, ele, n, num_open_p, num_closed_p)
  puts counter
  if num_closed_p == n
    res.push(ele)
    return
  end
  if (num_open_p == n)
    insert_p(res, ele + ')', n, num_open_p, num_closed_p+1)
  else
    if (num_open_p == num_closed_p)
      insert_p(res, ele + '(', n, num_open_p+1, num_closed_p)
    else
      insert_p(res, ele + '(', n, num_open_p+1, num_closed_p)
      insert_p(res, ele + ')', n, num_open_p, num_closed_p+1)
    end
  end
end


puts gen_p(3)