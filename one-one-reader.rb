=begin
The count-and-say sequence is the sequence of integers with the first five terms as following:

1.     1
2.     11
3.     21
4.     1211
5.     111221
1 is read off as "one 1" or 11.
11 is read off as "two 1s" or 21.
21 is read off as "one 2, then one 1" or 1211.
Given an integer n, generate the nth term of the count-and-say sequence.

Note: Each term of the sequence of integers will be represented as a string.

=end


def count(n)
  str = n.to_s
  running_count = 0
  new_str = ''
  str_index = 0
  i = 0
  while i < str.length do
    char = str[i]
    if i == 0
      cache = char
      running_count += 1
      i += 1
      next
    end
    if char == cache
      running_count += 1
    else
      new_str[str_index] = running_count.to_s
      str_index += 1
      new_str[str_index] = str[i-1]
      str_index += 1
      cache = char
      running_count = 1
    end
    i += 1
  end

  new_str[str_index] = (running_count).to_s
  new_str[str_index+1] = str[str.length-1]


  return new_str.to_i
end

def read(n)
  return 0 if n < 1
  return 1 if n == 1
  prev_val = read(n-1)
  return count(prev_val)
end


puts read(4)
puts read(5)