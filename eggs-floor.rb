=begin
Eggs can be very hard or very fragile, which means they may break if dropped from the first floor or
 may not even break if dropped from 100th floor. Both eggs are identical. You need to figure out
 the highest floor of a 100-story building an egg can be dropped without breaking.
The question is how many drops you need to make. You are allowed to break two eggs in the process.
=end


# let X be the first egg throw floor
#  minimize =  ((99/X).floor + (X-1))
#  x = 9, min = 10 + 8 = 18
#  x = 10, min = 9 + 9 = 18
# x = 11, min = 9 + 10
# x(x+1)/2 = 100
# x2 + x <= 200
# x2 + x -200 = 0
# x = 13.6
# 13,25,36,46,55,63,70,86,91,95,98,100