include Math

def multiply(a,b)
  return 0 if (a == 0 || b == 0)
  pow_a = log(a,2)
  pow_b = log(b,2)
  if pow_a.floor == pow_a
    return (b << pow_a.floor)
  end
  if pow_b.floor == pow_b
    return (a << pow_b.floor)
  end

  delta_a = a - (2 ** pow_a.floor)
  delta_b = b - (2 ** pow_b.floor)
  if delta_a < delta_b
    (b << pow_a.floor) + multiply(delta_a, b)    # (2floor + delta) * b
  else
    (a << pow_b.floor) + multiply(delta_b, a)
  end
end


puts multiply(33,3)


