# test binary tree properties
#properties to test
#one parent only
#no cycles. DAG


class Node
  def initialize(val,left, right)
    @val = val
    @right = right
    @left = left
    @visited = false
  end
  attr_accessor :val, :left, :right, :visited
end

# input : root (node) of a tree
# output : return fixed tree
def treeChecker(root)
  nodeHash = {}
  return root if root == nil
  root.visited = true
  dfs(root, nodeHash)
  root
end

def checkAndDeleteEdge(root, nodeHash)
  return nil if root == nil
  if nodeHash[root] || root.visited == true
    return nil
  else
    nodeHash[root] = true
    return root
  end
end



def dfs(root, nodeHash)
  return root if root == nil
  root.left = checkAndDelete(root.left, nodeHash)
  root.right = checkAndDelete(root.right, nodeHash)
  root.left.visited = true if root.left
  root.right.visited = true if root.right
  dfs(root.left, nodeHash)
  dfs(root.right, nodeHash)
end

=begin
test cases
nil
root pointing to itself
root with two nodes both pointing to one node
root with two nodes pointing to root
tree with many bad edges
run through a huge tree and benchmark time and space
just one root with no children

=end


