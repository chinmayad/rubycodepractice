=begin
Build Lowest Number by Removing n digits from a given number
Given a string ‘str’ of digits and an integer ‘n’, build the lowest possible number by removing ‘n’ digits from the string and not changing the order of input digits.

Examples:

Input: str = "4325043", n = 3
Output: "2043"

Input: str = "765028321", n = 5
Output: "0221"

Input: str = "121198", n = 2
Output: "1118"

=end

def lowestNumber(str,n, res='')
  len = str.length

  if n == 0
    res += str
    while ((res[0] == '0') && res[1])
      res.slice!(0)
    end
    return res
  elsif n >= len
    if res == ''
      return '0'
    else
      return res
    end
  else
    min = 10
    minI = -1
    for i in 0...(n+1) do # find lowest number, get index i
      if str[i].to_i < min.to_i
        min = str[i]
        minI = i
      end

    end
    res += min
    return lowestNumber(str[(minI+1)..-1], n-minI, res)
  end
end

puts lowestNumber("112",1)
puts lowestNumber("10200",1)
puts lowestNumber("765028321",5)
puts lowestNumber("",0)