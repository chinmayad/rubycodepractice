module BFS
  def findNode(val)
    return nil if val == nil
    dep = find(val)
    #self.clearVisited
    return dep
  end

  def find(id, depth=0)
    return depth if self.val == id
    self.visited
    self.children.each do |child|
      myQ.queue(child) unless child.visited?
    end
    while (myQ.size != 0)
      child = myQ.dequeue
      d = child.find(id, depth + 1)
      return d if d != nil
    end
    return nil
  end

  def myQ
    if not defined?(@que)
      @que = MyQueue.new
    else
      @que
    end
  end
  private :myQ
end