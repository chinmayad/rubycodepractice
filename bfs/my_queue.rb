
class MyQueue
  def initialize(size=50)
    @maxsize = size
    @q = []
  end

  def queue(val)
    return nil if @q.size >= @maxsize
    @q.push(val)
  end

  def dequeue
    return nil if @q.first == nil
    @q.shift
  end

  def size
    @q.size
  end
end