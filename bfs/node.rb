require_relative 'bfs-module'

class Node
  include BFS

  def initialize(val)
    @val = val
    @children = []
    @visited = false
  end

  def visited?
    @visited
  end

  def visited
    @visited = true
  end

  attr_accessor :val, :children
end