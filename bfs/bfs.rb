# Run the bfs search on a graph and find a node

require_relative 'my_queue'
require_relative 'Node'
require_relative 'graph'

# tests %%%%%%%%%%%%%%%%%%%%%%%%
h = { 0 => [1], 1 => [2], 2 => [0, 3], 3 => [2], 4 => [6], 5 => [4], 6 => [5] }
g = Graph.new(h)
list = g.getNodeList
root = list.first   # 0
ret = root.findNode(3)
puts ret
ret = root.findNode(4)
puts ret
puts 'coomplted test'
#########
#    6  <- 4        0 -> 1
#     -> 5 ^   3 <- ^ 2 <-
#
#