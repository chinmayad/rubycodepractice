class Graph
  def createNode(val)
    n = Node.new(val)
    @nodelist.push(n)
    n
  end

  def exists(c)
    getNodeList.each do |n|
      return n if n.val == c
    end
    return false
  end

  def parseGraph
    @nodeset.each do |key, val|
      keynode = createNode(key) unless keynode=exists(key)
      val.each do |child|
        childnode = createNode(child) unless (childnode = exists(child))
        keynode.children.push(childnode)
      end
    end
    true
  end

  def initialize(h)
    @nodeset = h
    @nodelist = []
    parseGraph
  end

  def getNodeList
    @nodelist
  end

  private :parseGraph, :createNode
end