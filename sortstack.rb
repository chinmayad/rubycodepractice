def sort(stack)
  if stack.size == 1
    return stack
  else
    entry = stack.pop
    stack = sort(stack)
    compare_and_insert(entry, stack)
  end
end

def compare_and_insert(e, stack)
  if stack.last > e
    e2 = stack.pop
    compare_and_insert(e,stack)
    stack.push(e2)
  else
    stack.push(e)
  end

  return stack
end

puts sort([1,5,3,2])