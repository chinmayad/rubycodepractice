# Given a binary tree, create a linked list of all nodes at each depth

require_relative 'slinkedlist'
require_relative 'genbinsearchtree'

class TNode
  def initialize(val, left=nil, right=nil)
    @val = val
    @left = left
    @right = right
  end
  attr_accessor :value, :left, :right
end

def inserttolist(depth, node)
  @listoflists = [] if not @listoflists
  @listoflists[depth] = SinglyLinkedList.new() if not @listoflists[depth]
  @listoflists[depth].insert(node)
end

def traversetree(root)
  depth = 0
  traverse(root, depth)
end

def traverse(root, depth)
  return if root == nil
  traverse(root.left, depth + 1)
  traverse(root.right, depth + 1)
  inserttolist(depth, root)
end

# Tests
a = [1,3,6,7,8]
root = genbinsearchtree(a)
traversetree(root)
puts 'complted test'