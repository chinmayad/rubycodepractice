=begin
Given an integer n, generate all structurally unique BST's (binary search trees) that store values 1...n.

For example,
Given n = 3, your program should return all 5 unique BST's shown below.

   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3

=end

# Definition for a binary tree node.
class TreeNode
  attr_accessor :val, :left, :right
  def initialize(val)
    @val = val
    @left, @right = nil, nil
  end
end

# @param {Integer} n
# @return {TreeNode[]}
def generate_trees(m=1,n)
  return [nil] if n < m
  res = []
  for i in m..n
    left = generate_trees(m,i-1)
    right = generate_trees(i+1,n)
    left.each do |l|
      right.each do |r|
        root = TreeNode.new(i)
        root.left = l
        root.right = r
        res.push(root)
      end
    end
  end
  res
end

h = generate_trees(2)
puts 'hi'