=begin
Given an iterator  with next and has_next, design an iterator_of_iterators

PS : Phase 2 solve this https://leetcode.com/problems/flatten-nested-list-iterator/description/

Given:
=end
class Iterator
  def initialize(list)
    @list = list
    @cur = -1
    @cur = 0 if not list.empty?
  end

  # returns true if next exists else false
  def has_next
    return true if @cur < @list.size
    return false
  end

  # returns next object if it exists or raises an error
  def next
    raise(Exception,'out of bounds') if @cur >= @list.size
    ret = @list[@cur]
    @cur = @cur + 1
    ret
  end
end

class IteratorOfIterators
  def initialize(nested)
    @nested = nested
    @curr = nil
    @curr = @nested.next if @nested.has_next
  end

  def has_next
    return false if not @curr
    return true
  end

  def get_next
    if @curr.has_next
      @curr.next
    else
      if @nested.has_next
        @curr = @nested.next
      else
        @curr = nil
      end
    end
  end

  def next
    raise(Exception, 'out of bounds') if not @curr
    ret = @curr
    get_next
    ret
  end
end

a = Iterator.new([1,2]) # a.next == 1; a.next == 2  Given : Iterator can iterate over a single list
puts a.next
puts a.has_next
puts a.next
puts a.has_next
b = Iterator.new([3])   # b.has_nest == true; b.next == 3.
puts b.has_next
puts b.next
c = Iterator.new([[1,2],3]) # c.next == [1,2], c.next == 3 , c.has_next == false
puts c.next
puts c.next
puts c.has_next
d = [Iterator.new([1,2]),Iterator.new([3])]
i = IteratorOfIterators.new(Iterator.new(d))
puts i.next
puts i.next
puts i.has_next
puts i.next
puts i.has_next