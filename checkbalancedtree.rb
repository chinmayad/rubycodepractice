# implement a function to check if a tree is balanced

require_relative 'genbinsearchtree'

a = [1,3,6,7,8]
root = genbinsearchtree(a)

def getdepth(root)
  return 0 if root == nil
  ldepth = getdepth(root.left)
  rdepth = getdepth(root.right)
  return false if Math.abs(ldepth - rdepth) > 1
  return max(ldepth, rdepth) + 1
end

def max(a,b)
  return a if a > b
  return b
end
