# create a min heap with two methods insert and extract_min

class MinHeap
  def initialize(*args)
    @heap = []
    @heap = args if args
  end

  def left(index)
    i = 2 * index + 1
    return nil if i >= @heap.size
    i
  end

  def right(index)
    i = 2 * index + 2
    return nil if i >= @heap.size
    i
  end

  def parent(index)
    i = (index - 1)/2
    return 0 if i < 0
    i
  end

  def heapify(i)
    return false if leaf_node?(i)
    min_i = min_among_children(i)
    return false if min_i == i
    swap(min_i, i)
    heapify(min_i)
    return true
  end

  def makeheap
    non_leafs = get_non_leafs()
    non_leafs.reverse.each do |n|
      heapify(n)
    end
    @heap
  end

  def insert(val)
    @heap.push(val)
    index = @heap.size - 1
    loop do
      index = parent(index)
      break if not heapify(index)
      break if (index == 0)
    end
  index
  end

  def pop
    val = @heap[0]
    swap(0, @heap.size - 1)
    @heap.delete_at(@heap.size - 1)
    heapify(0)
    val
  end
# private methods

  def get_non_leafs
    size = @heap.size
    return 0 if size == 0
    depth = Math.log2(size).floor
    return 0 if depth == 0
    last_non_leaf = (2 ** depth) - 2
    (0..last_non_leaf).to_a
  end

  def leaf_node?(i)
    return false if get_non_leafs.include?(i)
    return true
  end

  def min_among_children(i)
    max = i
    max = (@heap[i] < @heap[left(i)]) ? i : left(i)  if left(i)
    max = (@heap[max] < @heap[right(i)]) ? max : right(i)  if right(i)
    max
  end

  def swap(a,b)
    temp = @heap[a]
    @heap[a] = @heap[b]
    @heap[b] = temp
  end

  private :min_among_children, :leaf_node?, :left, :right, :parent
end

h = MinHeap.new(7, 8, 15, 13, 44, 3, 14, 1, 17, 6, 9, 0)
k = h.makeheap
h.insert(2)
min = h.pop
puts 'completed test'

