def treePrinter(root)
  return nil if root == nil
  queue = Array.new
  queue.unshift(0)
  queue.unshift(root)

  while node = queue.pop do
    return if queue.size == 0
    if node.is_a?(Integer)
      puts '---------------------'
      puts "nodes at depth #{node}"
      queue.unshift(node+1)
      next
    end
    puts node.value
    node.children.each do |child|
      queue.unshift(child)
    end
  end
end


def levelPrinter(root)
  return nil if root == nil
  q = Array.new
  q.unshift(root)
  while (!q.empty?) do
    nCount = q.size
    puts '----------------------'
    while (nCount > 0)
      node = q.pop
      nCount -= 1
      puts node.value
      node.children.each do |c|
        q.unshift(c)
      end
    end
  end
end

class Node
  def initialize(value, children=[])
    @value = value
    @children = children
  end

  attr_accessor :value, :children
end

tree = Node.new(1,[Node.new(2,[Node.new(4, [Node.new(8)])]), Node.new(3, [Node.new(5)])])
treePrinter(tree)
levelPrinter(tree)