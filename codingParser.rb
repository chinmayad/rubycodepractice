'''
You’re given an array of CSV strings representing search results. Results are sorted by a score initially. A given host may have several listings that show up in these results.
1. Suppose we want to show 12 results per page, but we don’t want the same host to dominate the results. Write a function that will reorder the list so that
2. a host shows up at most once on a page if possible,
3. but otherwise preserves the ordering. Your program should return the new array and print out the results in blocks representing the pages.

at most 6 results per page

"1,28,300.1,San Francisco",
"4,5,209.1,San Francisco",
"20,7,208.1,San Francisco",
"23,8,207.1,San Francisco",
"16,10,206.1,Oakland",
"1,16,205.1,San Francisco",
"1,31,204.6,San Francisco",
"6,29,204.1,San Francisco",
"7,20,203.1,San Francisco",

page1
"1,28,300.1,San Francisco",
"4,5,209.1,San Francisco",
"20,7,208.1,San Francisco",
"23,8,207.1,San Francisco",
"16,10,206.1,Oakland",
"6,29,204.1,San Francisco",

page2
"1,16,205.1,San Francisco",
"7,20,203.1,San Francisco",

page3
"1,31,204.6,San Francisco",


"1,31,204.6,San Francisco",
"1,31,204.6,San Francisco",
"1,31,204.6,San Francisco",
"1,31,204.6,San Francisco",

hash 1 "1,31,204.6,San Francisco",
hash 2 "1,31,204.6,San Francisco",
hash 3
"1,31,204.6,San Francisco",

"1,31,204.6,San Francisco", => 3
<key, last page>
key? host_id

'''
def csvParse(results, pageSize=12)
  entryHash = {}
  pages = []
  return pages if results == nil
  results.each do |r|
    id,score,distance,city = processInput(r)
    if p = entryHash[id]
      pageAdded = addToPage(pages, p+1, r, pageSize)
      entryHash[id] = pageAdded
    else
      pageAdded = addToPage(pages, 0, r, pageSize)
      entryHash[id] = pageAdded
    end
  end
  pages
end


def processInput(r)
  raise 'bad input' if r == nil
  ips = r.split(',')
  raise 'wrong number of entries' if ips.size != 4
  return ips
end

def addToPage(pages, p, r, pageSize)
  if pages[p]
    if pages[p].size < pageSize
      pages[p].push(r)
      return p
    else
      return addToPage(pages, p+1, r, pageSize)
    end
  else
    pages.push([r])
    return p
  end
end

def printer(results)
  results.each do |r|
    puts r
    puts '*********************'
  end
  puts '-----------------------------------'
end




res = [
    "1,28,300.1,San Francisco",
    "4,5,209.1,San Francisco",
    "20,7,208.1,San Francisco",
    "23,8,207.1,San Francisco",
    "16,10,206.1,Oakland",
    "1,16,205.1,San Francisco",
    "1,31,204.6,San Francisco",
    "6,29,204.1,San Francisco",
    "7,20,203.1,San Francisco",
    "8,21,202.1,San Francisco",
    "2,18,201.1,San Francisco",
    "2,30,200.1,San Francisco",
    "15,27,109.1,Oakland",
    "10,13,108.1,Oakland",
    "11,26,107.1,Oakland",
    "12,9,106.1,Oakland",
    "13,1,105.1,Oakland",
    "22,17,104.1,Oakland",
    "1,2,103.1,Oakland",
    "28,24,102.1,Oakland",
    "18,14,11.1,San Jose",
    "6,25,10.1,Oakland",
    "19,15,9.1,San Jose",
    "3,19,8.1,San Jose",
    "3,11,7.1,Oakland",
    "27,12,6.1,Oakland",
    "1,3,5.1,Oakland",
    "25,4,4.1,San Jose",
    "5,6,3.1,San Jose",
    "29,22,2.1,San Jose",
    "30,23,1.1,San Jose"
]

res = [
    "1,28,300.1,San Francisco",
    "4,5,209.1,San Francisco",
    "20,7,208.1,San Francisco",
    "23,8,207.1,San Francisco",
    "1,10,206.1,Oakland"]

p csvParse(res)

