=begin
Given a set of numbers in an array which represent a number of consecutive days of Airbnb reservation requested,
 as a host, pick the sequence which maximizes the number of days of occupancy, at the same time,
leaving at least a 1-day gap in-between bookings for cleaning.
The problem reduces to finding the maximum sum of non-consecutive array elements.
E.g.
// [5, 1, 1, 5] => 10
The above array would represent an example booking period as follows -
// Dec 1 - 5
// Dec 5 - 6
// Dec 6 - 7
// Dec 7 - 12

The answer would be to pick Dec 1-5 (5 days) and then pick Dec 7-12 for a total of 10 days of
occupancy, at the same time, leaving at least 1-day gap for cleaning between reservations.

Similarly,
// [3, 6, 4] => 7
// [4, 10, 3, 1, 5] => 15
=end

def maxDays(bookings)
  maxp1 = 0
  maxp2 = 0
  maxp3 = 0
  bookings.each do |b|
    curMax = max(b+maxp2, b+maxp3, maxp1)
    maxp3 = maxp2
    maxp2 = maxp1
    maxp1 = curMax
  end
  return maxp1
end

def max(a,b,c)
  res1 = a > b ? a : b
  res1 > c ? res1 : c
end

puts maxDays([5, 1, 1, 5])
puts maxDays([1, 5, 1, 1, 5])
puts maxDays([3,6,4])
puts maxDays([4,10,3,1,5])
