=begin
Given an array consists of non-negative integers, your task is to count the number of triplets chosen from the array that can make triangles if we take them as side lengths of a triangle.

Example 1:
Input: [2,2,3,4]
Output: 3
Explanation:
Valid combinations are:
2,3,4 (using the first 2)
2,3,4 (using the second 2)
2,2,3
Note:
The length of the given array won't exceed 1000.
The integers in the given array are in the range of [0, 1000].
=end

def triangle_number(nums, edge1=[], edge2=[], edge3=[])
  sum = 0
  cache = {}
  for i in 0...nums.size do
    for j in i+1...nums.size do
      for k in j+1...nums.size do
        if isTriangle(nums[i],nums[j],nums[k],cache)
          sum += 1
        end
      end
    end
  end
  sum
end

def isTriangle(a,b,c, cache)
  return cache[[a,b,c]] if cache[[a,b,c]]
  if a == [] or b == [] or c == []
    res = false
  elsif ((a + b > c) && (b + c > a) && (c + a > b))
    res = true
  else
    res = false
  end
  cache[[a,b,c]] = res
  return res
end

puts triangle_number([2,2,3,4])