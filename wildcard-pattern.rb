=begin
'?' Matches any single character.
'*' Matches any sequence of characters (including the empty sequence).

The matching should cover the entire input string (not partial).

The function prototype should be:
bool isMatch(const char *s, const char *p)

Some examples:
isMatch("aa","a") → false
isMatch("aa","aa") → true
isMatch("aaa","aa") → false
isMatch("aa", "*") → true
isMatch("aa", "a*") → true
isMatch("ab", "?*") → true
isMatch("aab", "c*a*b") → false
=end

# "aababc", "a*b*c"

def checkForStar(p)
  p.each_char do |c|
    return false if c != '*'
  end
  return true
end

@hash = {}

def isMatch(s, p)
  #return @hash[[s,p]] if @hash[[s,p]]
  if s == ''       # if s is done at this point, return true if p is too
    ret = (p == '' || checkForStar(p))
    #@hash[[s,p]] = ret
    return ret
  end

  if p[0] == '?'    # Match a single character and move on
    ret = isMatch(s[1..-1], p[1..-1])
    #@hash[[s,p]] = ret
    return ret
  end

  if p[0] == '*'    # try all possible non special matches ahead if a * is found.
    index = 1
    while (index < p.length) do   # coagulate all * to improve speed
      break if (p[index] != '*')
      index += 1
    end

    return true if index == p.length   # There are no a-z characters in p anymore. Only * . So its a match
    (0...s.length).each do |i|  # for every match of a non-special character between s and p, try moving forward
      return true if isMatch(s[i..-1], p[index..-1])
    end
    #@hash[[s,p]] = false
    return false
  end

  if s[0] == p[0]
    ret = isMatch(s[1..-1], p[1..-1])
    #@hash[[s,p]] = ret
    return ret
  end
  #@hash[[s,p]] = false
  return false
end
start = Time.now
puts '1pass' if false == isMatch("aaaaaaaaaaaaab" ,"a*a*a*a*a*a*a*a*a*a*c")
@hash = {}
puts '2pass!' if true == isMatch("aa","aa")
@hash = {}
puts '3pass!' if false == isMatch("aaa","aa")
@hash = {}
puts '4pass!' if true == isMatch("aa", "a*")
@hash = {}
puts '5pass!' if false == isMatch("aba", "*b*b*")
@hash = {}
puts '6pass!' if true == isMatch("ab", "?*")
@hash = {}
puts '7pass!' if false == isMatch("aab", "c*a*b")
@hash = {}
puts '8pass!' if false == isMatch("b","*?*?")
@hash = {}
puts '9pass!' if false == isMatch("aabb","*?aa")
@hash = {}
puts '10pass!' if false == isMatch("aa","?aa")
@hash = {}
puts '11pass!' if true == isMatch("aaa","aa?*")
@hash = {}
puts '12pass!' if true == isMatch("aa", "****")
@hash = {}
puts '13pass!' if false == isMatch("aa", "a*bc")
@hash = {}
puts '14pass!' if true == isMatch("ab", "ab*")
@hash = {}
puts '15pass!' if true == isMatch("ho", "ho**")
@hash = {}
puts '16pass!' if true == isMatch("aababc", "a*b*c")
@hash = {}
puts '17pass!' if false == isMatch("babbbaabbaaaaabbababaaaabbbbbbbbbbabbaaaabbababbabaa" ,"**a****a**b***ab***a*bab")
puts '************************'
finish = Time.now
puts finish-start