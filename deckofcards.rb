require 'socket'

class Card
  def initialize(suite, digit)
    @suite = suite
    @digit = digit
    @value = absvalue(digit)
  end

  def absvalue(x)
    return x.to_i if (x.to_i > 1 && x.to_i < 11)
    return 10 if x == 'K' || x == 'Q' || x == 'J'
    return 11 if x == 'A'
  end

  attr_accessor :suite, :value, :digit
end


class Deck
  NUM_CARDS_IN_ONE_DECK = 52
  SUITE = %w(heart spade clover diamond)
  NUMBERS = %w(A 1 2 3 4 5 6 7 8 9 10 J Q K)

  def initialize(n=1)
    @num_of_decks = n
    @cards = []
    i = 0
    while (i < n)
      generatedeck
      i += 1
    end
  end

  def generatedeck
     SUITE.each do |s|
       NUMBERS.each do |n|
         @cards.push(Card.new(s, n))
       end
     end
  end

  def cards(n=1)
    @cards.pop(n)
  end

  def shuffle
    @cards.shuffle!
  end

end

def check_value(clist)
  sum = 0
  clist.each do |c|
    sum += c.value
  end
  sum
end

class BlackJack < Deck
  def initialize(n=1)
    super
  end

  def deal
    cards.pop
  end
end

bj = BlackJack.new
bj.shuffle

t = TCPServer.new('127.0.0.1', 8000)
while (conn = t.accept) do
    th = Thread.new(conn) do |c|
      text = ''
      my_cards = []
      loop do
        card = bj.deal
        c.puts "#{card.digit} of #{card.suite}"
        my_cards.push(card)
        c.puts 'type hit or stand'
        text = c.gets.chomp
        break if text != 'hit'
      end
        if text == 'stand'
          c.puts check_value(my_cards)
        else
          c.puts 'wrong input'
        end
    end
  th.join
  puts 'im here'
end