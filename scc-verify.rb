=begin
A directed graph is strongly connected if there is a path between all pairs of vertices.
A strongly connected component (SCC) of a directed graph is a maximal strongly connected subgraph.
Another way to define a SCC is to say that there exists a path from every vertex to every other vertex in SCC.
check whether a given graph is SCC
=end

class Node
  def initialize(id)
    @id = id
    @visited = false
  end

  attr_accessor :id, :visited
end

def checkSCC(graph)
  idToNode = createNodes(graph)
  dimension = idToNode.size
  m = getAdjacencyMatrix(graph, dimension)
  dfs(0, idToNode, m)
  return false if verifyAllVisited(idToNode) == false
  m = reverseEdges(m)
  dfs(0, idToNode, m)
  verifyAllVisited(idToNode)
end

def createNodes(hash)
  nodes = {}
  hash.each do |key,val|
    if not nodes[key]
      n = Node.new(key)
      nodes[key] = n
    end
    val.each do |v|
      if not nodes[v]
        n = Node.new(v)
        nodes[v] = n
      end
    end
  end
  nodes
end

def getAdjacencyMatrix(hash, d)
  m = Array.new(d) { Array.new(d) { 0 } }
  hash.each do |key, val|
    val.each do |v|
      m[key][v] = 1
    end
  end
  m
end

def dfs(root_id, nodeList, edges)
  return if  !root_id || !nodeList || !edges
  root = nodeList[root_id]
  root.visited = true
  children(root_id, edges).each do |c|
    dfs(c,nodeList,edges) unless nodeList[c].visited
  end
end

def children(id, edges)
  row = edges[id]
  res = []
  row.each_with_index do |r, index|
    res.push(index) if r == 1
  end
  res
end

def verifyAllVisited(nodeList)
  nodeList.each do |key,val|
    return false if val.visited == false
    val.visited = false
  end
  true
end

def reverseEdges(edges)
  edges.transpose
end

h = { 0 => [1], 1 => [2], 2 => [0, 3], 3 => [2], 4 => [6], 5 => [4], 6 => [5] }
puts checkSCC(h)
h = {0 => [1], 1 => [2], 2 => [3]}
puts checkSCC(h)
h = {0 => [1], 1 => [2], 2 => [3], 3 => [0]}
puts checkSCC(h)