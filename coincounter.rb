# given infinite 25 cents, 10 cents , 5 cents and 1 cent. figure out number of ways to represent n cents

class CoinCounter
  def initialize(n)
    @n = n
    @ways = 0
  end

  def inc_ways(val = 1)
    @ways = @ways + val
  end

  def calcAll5(n)
    z = (n/5).floor
    inc_ways(z.to_i)     # 5 * z + 1s , for each 5 upto z. Ex : 12 => z = 2, (5,5,1s) (5,1s)
  end

  def calcAll10(n)
    # only 10s 5s and 1s
    y = (n/10).floor
    inc_ways(y.to_i)     # 10 * y + 1s, for each 10 upto y
    i = 1
    while (i <= y) do
      calcAll5(n - (i * 10))
      i = i + 1
    end
  end

  def calcAll25(n)
    # only 25s 10s 5s and 1s
    x = (n/25).floor
    inc_ways(x.to_i)     # 10 * y + 1s, for each 10 upto y
    i = 1
    while (i <= x) do
      calcAll5(n - (i * 25))
      i = i + 1
    end
  end

  def getPossibilities
    return if @n == 0
    inc_ways if @n >= 1      # for all 1s
    calcAll5(@n)       # only 5s and 1s
    calcAll10(@n)      # only 10s 5s and 1s
    calcAll25(@n)      # only 25s, 10s, 5s, and 1s
  end

  attr_reader :ways
end

class Coiner
  def initialize(n=nil, arr=nil)
    @n = n
    @arr = arr.sort
    @ways = 0
  end

  def inc_ways(val=1)
    @ways += val
  end

  def find_ways(e)
    quo = @n/e
    inc_ways if quo * e == @n
  end

  def coiner
    return nil if (@arr == nil or n == nil)
    @arr.each do |e|
      quo = @n/e
      while (quo != 0) do
        find_ways(@n - quo*e)
      end
    end
  end
end

cc = CoinCounter.new(15)
cc.getPossibilities

def collector(coins,n, set=[], res=[])
  if n == 0
    res.push(set)
    return 1
  end
  return 0 if n < 0
  ways = 0
  coins.each do |coin|
    next if (!set.empty? && coin > set.last)
    ways += collector(coins, n-coin, set + [coin], res)
  end
  ways
end

coins = [1,10,5,25]
res = []
puts collector(coins,15, [], res)
p res