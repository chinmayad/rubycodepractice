=begin
The second question was given a sorted array that has been rotated, find a number in that array {6,7,1,2,3,4,5}
=end

def inBetween(target, low, high)
  return true if target >= low && target <= high
  false
end

def findNumber(nums,target,low=0, high=nums.size-1)
  return -1 if low > high
  med = (low + high)/2
  return med if nums[med] == target
  if inBetween(target,nums[med], nums[high])
    return findNumber(nums,target,med+1,high)
  elsif inBetween(target,nums[low], nums[med])
    return findNumber(nums,target,low,med-1)
  else
    if nums[med] > nums[high]
      return findNumber(nums,target,med+1,high)
    elsif nums[med] < nums[low]
      return findNumber(nums,target,low,med-1)
    else
      return -1
    end
  end
end

def binarySearch(nums, target,low=0, high=nums.size-1)
  return false if nums.size == 0 || target > nums[high] || target < nums[low] || low > high
  med = (low+high)/2
  return true if target == nums[med]
  if target > nums[med]
    return binarySearch(nums, target, med+1,high)
  else
    return binarySearch(nums, target, low,med-1)
  end
end


nums2 =  [6,7,1,2,3,4,5]
nums = [1,3,5]
nums3 =  [1,2,3,4,6]
k = Time.now
#puts findNumber([41,42,45,46,57,58,59,62,63,64,66,70,71,72,74,77,81,84,86,88,89,94,98,99,1,3,5,7,9,11,13,15,17,19, \
# 21,23, 25,27, 29,33,34,36,36,37,39],99)
puts findNumber([1,3],3)
puts Time.now - k