=begin
given nxn puzzle where 1..(n^2-1) elements r ordered randomly. order them in order 1...   by sliding elements only.
                                                                                   ....
                                                                                   ...n
Count min num of ways to getting there

ex : [1,3]
     [2 .]
ans : [1  2
       3  .]





[1,2,3]   [1,2,3]
[4,-,6]   [4,5,6]
[7,8,5]   [7,8,-]

[1,2,3]
[4,6,-]
[7,8,5]

[1,2,3]
[4,6,5]
[7,8,-]

[1,2,3]
[4,6,5]
[7,-,8]

[1,2,3]
[4,-,5]
[7,6,8]

[1,2,3]
[4,5,-]
[7,6,8]

[1,2,3]
[4,5,8]
[7,6,-]
=end

def slidingPuzzle3x3(m)
  eIndex = findEmptySpot(m)
  queue = []
  bfs(m.dup, queue)
end

def bfs(m, queue)
  return [m] if checkForSoln(m)
  root = findEmpty(m)
  neighbors(m,root).each do |coord|
    queue.push(swap(m,root,coord))
  end
  while coord = queue.unshift
    res = bfs(m,root, coord[0], queue)
    return [m] + res if res != nil
  end
  return nil
end

def checkForSoln(m)
  i = 1
  m.each do |r|
    r.each do |c|
      return false if m[r][c] != i
      i += 1
    end
  end
  return true
end

