require_relative 'slinkedlist'

# given two linked list , add them
# 6 -> 1 -> 7   (617)
# 2 -> 9 -> 5   (295)  +
# 9 -> 1 -> 2   (912)    Must be the answer

in1 = SinglyLinkedList.new()
in1.insert(7)
in1.insert(1)
in1.insert(6)

in2 = SinglyLinkedList.new()
in2.insert(5)
in2.insert(9)
in2.insert(2)

def zeropad(ll, len)
  while (ll.size < len)
    ll.insert(0)
  end
end

def decimaladder(v1, v2, v3)
  return (v1 + v2 + v3) % 10, (v1 + v2 + v3) / 10
end

def adder(suml, n1, n2)
  return 0 if (n1 == nil || n2 == nil)
  xcarry = adder(suml, n1.nex, n2.nex)
  res, carry = decimaladder(n1.val, n2.val, xcarry)
  suml.insert(res)
  return carry
end

def summer(in1, in2)
  l1 = in1.size
  l2 = in2.size
  zeropad(in1, l2 - l1) if (l1 < l2)
  zeropad(in2, l1 - l2) if (l2 < l1)
  suml = SinglyLinkedList.new()
  carry = adder(suml, in1.head, in2.head)
  return suml if carry == 0
  suml.insert(carry)
  suml
end

sum = summer(in1, in2)
puts 'compelted test'



