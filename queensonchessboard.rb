=begin
https://leetcode.com/problems/n-queens/description/
=end

def findQueens(board,n)
  res = []
  checkQueens(board,0,res,n)
  res
end

def deepCopy(board)
  newArr = []
  board.each do |b|
    newArr.push(b.join)
  end
  newArr
end

def checkQueens(board, c, res, n)
  for r in 0...n do
    if placeQueen(board,r,c,n)     # placeQueen places a queen at board[r,i] and checks if any queens can
      if c == n-1          # can see this queen horizontally vertically or diagonally,returns true if valid else false
        if not res.include?(board)    # (and removes queen)
          res.push(deepCopy(board))
        end
        removeQueen(board,r,c)
        return
      else
        checkQueens(board,c+1,res,n)
        removeQueen(board,r,c)
       end
    end

  end
end

def removeQueen(board,r,i)
  board[r][i] = '.'
end

def placeQueen(board, r, c,n)
  if validRow(board, r,n) && validCol(board,c,n) && validDiag(board,r,c,n)
    board[r][c] = 'Q'
    return true
  end
  false
end

def validRow(board,r,n)
  for i in 0...n do
    return false if board[r][i] == 'Q'
  end
  true
end

def validCol(board,c,n)
  for i in 0...n do
    return false if board[i][c] == 'Q'
  end
  true
end

def validDiag(board,r,i,n)
  j = r ;k = i
  while (j < n) && (k < n) do
    return false if board[j][k] == 'Q'
    j += 1
    k += 1
  end

  j = r; k = i
  while (j < n) && (k >= 0) do
    return false if board[j][k] == 'Q'
    j += 1
    k -= 1
  end

  j = r; k = i
  while (j >= 0) && (k >= 0) do
    return false if board[j][k] == 'Q'
    j -= 1
    k -= 1
  end

  j = r; k = i
  while (j >= n) && (k < n) do
    return false if board[j][k] == 'Q'
    j -= 1
    k += 1
  end
  true
end

n = 4
board = Array.new(n) { Array.new(n) { '.' }}
a = findQueens(board, n)
a.each do |b|
  p b
  puts '----------'
end
