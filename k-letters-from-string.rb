=begin
K lettered words from a string
Print all possible strings of length k that can be formed from a set of n characters
2.5
Given a set of characters and a positive integer k, print all possible strings of length k
that can be formed from the given set.
http://www.geeksforgeeks.org/print-all-combinations-of-given-length/
=end

def kWords(str, k)
  return nil if str == nil
  return '' if str == ''
  res = []
  str.each_char {|c| res.push(c)}
  k -= 1
  while k > 0
    res = insert(str,res)
    k -= 1
  end
  res
end

def insert(str, res)
  res2 = []
  str.each_char do |c|
    res.each do |r|
      res2 << r + c
    end
  end
  res2
end


p kWords('abc', k=2)
p kWords('ab',k=3)
=begin
{a,b} k = 2
aa
ab
ba
bb

a, b
aa,ba # insert at end always
ab,bb # same

{a,b,c}
a,b,c
aa,ba,ca,ab,bb,cb,ac,bc,cc

{a,b} k = 3
a,b
aa,ba,ab,bb
aaa,baa,aba,bba,aab,bab,abb,bbb
=end

#{ab}
# []
# ['', a]
# ['',a,b,ab]




