# small string s
# big string b
# all permutations of s in b with index.


# s = 'abcd'
# b = 'gefwefwevcvxcdccbacdab'
#

def permuter(b, s)
  # Error checks b.len > s.len, non-null
  s_hash = {}
  s.split("").each do |element|                 #O(s)
    s_hash[element] = 0
  end
  cnt  = 0
  while (cnt < (b.length - s.length)) do
    if s_hash.has_key?(b[cnt])
      b_hash = {}
      s_cnt = 0
      n_cnt = cnt
      while (s_cnt < s.length && n_cnt < b.length) do
        if not s_hash.has_key?(b[n_cnt])
          cnt = n_cnt
          break
        end
        if b_hash.has_key?(b[n_cnt])
          b_hash.delete(b[n_cnt])
          s_cnt -= 1
          break
        end
        b_hash[b[n_cnt]] = 0
        s_cnt += 1
        n_cnt += 1
      end
      if (s_cnt == s.length)
        puts "Found permutation " + b_hash.keys.join("")
        puts "At index #{cnt}"
      end
    end
    cnt += 1
  end
end

s1 = 'abcd'
b1 = 'gefwefwevcvxcdccbacdabq'
permuter(b1,s1)