=begin
https://leetcode.com/problems/flatten-nested-list-iterator/description/

Given a nested list of integers, implement an iterator to flatten it.

Each element is either an integer, or a list -- whose elements may also be integers or other lists.

Example 1:
Given the list [[1,1],2,[1,1]],

By calling next repeatedly until hasNext returns false, the order of elements returned by next should be: [1,1,2,1,1].

Example 2:
Given the list [1,[4,[6]]],

By calling next repeatedly until hasNext returns false, the order of elements returned by next should be: [1,4,6].

# Your NestedIterator will be called like this:
# i, v = NestedIterator.new(nested_list), []
# while i.has_next()
#    v << i.next
# end
=end

class NestedIterator
  def initialize(list)
    @flattenedList = flattener(list)
    @ptr = 0
  end

  def flattener(list)
    return [] if list == nil
    return [list] if list.is_a?(Integer)
    res = []
    list.each do |e|
      res += flattener(e)
    end
    res
  end

  def has_next
    return true if @ptr < @flattenedList.size
    false
  end

  def next
    if self.has_next
      ret = @flattenedList[@ptr]
      @ptr += 1
      ret
    end
  end
end

n = NestedIterator.new([1,[4,[6]]])
while n.has_next()
  puts n.next
end
puts '------------'
n = NestedIterator.new()
while n.has_next()
  puts n.next
end

