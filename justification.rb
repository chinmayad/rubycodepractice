=begin
Given an array of words and a length L, format the text such that each line has exactly L characters and is fully (left and right) justified.

You should pack your words in a greedy approach; that is, pack as many words as you can in each line. Pad extra spaces ' ' when necessary so that each line has exactly L characters.

Extra spaces between words should be distributed as evenly as possible. If the number of spaces on a line do not divide evenly between words, the empty slots on the left will be assigned more spaces than the slots on the right.

For the last line of text, it should be left justified and no extra space is inserted between words.

For example,
words: ["This", "is", "an", "example", "of", "text", "justification."]
L: 16.

Return the formatted lines as:
[
   "This    is    an",
   "example  of text",
   "justification.  "
]
Note: Each word is guaranteed not to exceed L in length.
=end

def full_justify(words, max_width,res = [])
  return res if words.empty?
  rc = 0
  wc = 0
  i = 0
  words.each do |word|
    i += 1
    l = word.length

    if (rc + l) < max_width
      wc += 1
      rc =rc + l + 1
    elsif (rc + l == max_width)
      wc += 1
      sentence = words[0...wc].join(' ')
      res.push(sentence)
      full_justify(words[wc..-1], max_width, res)
      return res
    else
      num_spaces = max_width - rc + (wc - 1)
      space_per_word = 0
      left_word_justify = 0
      space_per_word = num_spaces/(wc-1) unless wc == 1
      left_word_justify = num_spaces%(wc-1) unless wc == 1
      spaces = ''
      space_per_word.times do
        spaces += ' '
      end
      left_spaces = ''
      left_word_justify.times do
        left_spaces += ' '
      end
      sentence = ''
      words[0..wc].each_with_index do |word, index|
        sentence += word
        break if index == wc - 1
        sentence += spaces
        sentence += left_spaces if index == 0
      end
      res.push(sentence)
      full_justify(words[wc..-1], max_width, res)
      return res
    end
  end
  if i == words.size
    sentence = words[0..-1].join(' ')
    res.push(sentence)
  end
  return res
end

words = ["This", "is", "an", "example", "of", "text", "justification."]
puts full_justify(words,16)

words = [""]
puts full_justify(words,16)

words = ["Listen","to","many,","speak","to","a","few."]
puts full_justify(words,6)