module MergeSort
  def sortArray
    sort(self)
  end

  def sort(a)
    if (a.size > 1)
      len = a.size
      half = ((len-1)/2).floor
      s1 = sort(a[0..half])
      s2 = sort(a[(half+1)..len-1])
      return merge(s1,s2)
    else
      return a
    end
  end

  def merge(s1, s2)
    l1 = s1.size
    l2 = s2.size
    result = []
    i = 0; j = 0; k = 0
    while (i < l1) && (j < l2) do
      if s1[i] < s2[j]
        result[k] = s1[i]
        i = i + 1
      else
        result[k] = s2[j]
        j = j + 1
      end
      k = k + 1
    end

    if i == l1
      while (j < l2) do
        result[k] = s2[j]
        j = j + 1
        k = k + 1
      end
    else
      while (i < l1) do
        result[k] = s1[i]
        i = i + 1
        k = k + 1
      end
    end
    result
  end
end


arr = [1,7,5,3,2,8,4]
arr.extend(MergeSort)
puts arr.sortArray