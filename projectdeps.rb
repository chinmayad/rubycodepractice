#Given a proj and its dependencies, find the right order of building projects
# ex : projs :a,b,c,d,e,f
# deps : d->a, f->b, b->d, f->a, d->c  (d depends on a)
# soln : f,e,a,b,d,c

require_relative 'slinkedlist'

class Queue
  def dequeue
    begin
      return self.pop(non_block=true)
    rescue ThreadError
      return nil
    end
  end

  def enqueue(n)
    self.push(n)
  end
end


module BFS
  def pending_nodes
    @pending_nodes = Queue.new() if not @pending_nodes
    @pending_nodes
  end

  def find(n)
    pending_nodes.enqueue(self)
    while (node = @pending_nodes.dequeue) do
      next if visited?(node)
      return node if node.value == n
      node.children.each do |child|
        @pending_nodes.enqueue(child)
      end
    end
  end

  def visited_nodes
    @visited_nodes = [] if not @visited_nodes
    @visited_nodes
  end

  def visited?(n)
    return true if visited_nodes.include?(n)
    visited_nodes.push(n)
    return false
  end
end

class Node
  include BFS
  def initialize(val, *kids)
    @value = val
    @children = kids
    @visited = false
  end
  attr_accessor :value, :children, :visited
end

class Graph
  def initialize(hash)
    @hashform = hash
    @node_list = {}
    hash.each do |key, value|
      n = find_node(key)
      next if value == nil
      value.each do |child|
        c = find_node(child)
        n.children.push(c) if not n.children.include?(c)
      end
    end
    @node_list
  end

  def find_node(n)
    return @node_list[n] if @node_list[n]
    node = Node.new(n)
    @node_list[n] = node
    node
  end

  def size
    @node_list.size
  end

  attr_reader :node_list
end

# tests %%%%%%%%%%%%%%%%%%%%%%%%
h = { 'e' => ['a', 'b'], 'a' => ['b', 'c', 'f'], 'b' => ['f'], 'c' => ['f'], 'f' => nil, 'g' => ['d'], 'd' => nil }
g = Graph.new(h)
list = g.node_list

def ll
  @ll = SinglyLinkedList.new() if not @ll
  @ll
end

def mark(n)
  n.visited = true
end

def mark_and_insert(n)
  return if n.visited
  mark(n)
  ll.insert_node(n)
end

def traverse_and_insert(n)
  if n.children.size == 0
    mark_and_insert(n)
  else
    n.children.each do |c|
      next if c.visited
      traverse_and_insert(c)
    end
    mark_and_insert(n)
  end
end

list.each do |key, val|
  traverse_and_insert(val)
end


puts 'coomplted test'
#########
#    6  <- 4        0 -> 1
#     -> 5 ^   3 <- ^ 2 <-
#
#