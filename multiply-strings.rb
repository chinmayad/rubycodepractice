=begin
Given two non-negative integers num1 and num2 represented as strings, return the product of num1 and num2.

Note:

The length of both num1 and num2 is < 110.
Both num1 and num2 contains only digits 0-9.
Both num1 and num2 does not contain any leading zero.
You must not use any built-in BigInteger library or convert the inputs to integer directly.
=end


# @param {String} num1
# @param {String} num2
# @return {String}
def multiply(num1, num2)
  cache = {}
  total = 0
  if num2.size > num1.size
    temp = num1
    num1 = num2
    num2 = temp
  end
  num2.each_char do |n2|
    total *= 10
    total += multi(num1, n2, cache)
  end
  total.to_s
end

def multi(num1, n2, cache)
  return cache[n2] if cache[n2]
  res = 0
  num1.each_char do |n1|
    res *= 10
    res += (n2.to_i * n1.to_i)
  end
  cache[n2] = res
end