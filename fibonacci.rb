# gen fibonacci numbers upto n
# 1 1 2 3 5 8 13 21 34...
# 0 1 2 3 4 ..

def cachevar
  @cachevar = [1,1] unless @cachevar
  @cachevar
end

def fastfibonacci(n)
  return 1 if n == 1 || n == 2
  return cachevar[n-1] if n <= cachevar.size
  x = fastfibonacci(n-1) + fastfibonacci(n-2)
  cachevar[n-1] = x
  x
end


def slowfibonacci(n)
  return 1 if n == 1 || n == 2
  slowfibonacci(n-1) + slowfibonacci(n-2)
end

btime = Time.now

puts fastfibonacci(40)
etime = Time.now

puts etime - btime

p cachevar