=begin
Given an unsorted integer array, find the first missing positive integer.

For example,
Given [1,2,0] return 3,
and [3,4,-1,1] return 2.

Your algorithm should run in O(n) time and uses constant space.
=end

def swap(n, i)
  num = n[i]
  return if num == n[num-1]
  temp = n[num-1]
  n[num-1] = num
  n[i] = temp
  swap(n, i) if valid(n, n[i])
end

def valid(n, num)
  return ((num <= n.size) && (num > 0))
end

def first_positive(n)
  max = 0
  index = 0
  while index < n.size do
    num = n[index]
    if num <= 0
      index += 1
      next
    end
    max = num if num > max
    swap(n, index) if valid(n, n[index])
    index += 1
  end
  index = 0
  while (index < n.size) do
    return (index + 1) if n[index] != index + 1
    index += 1
  end
  return max + 1
end

=begin
# [1, 3, 3, 4]
actual_sum = 11
true_sigma_max_num = 10
positive_num_sum = 10
=end


puts first_positive([1,2,0])
puts first_positive([3,4,-1,1])
puts first_positive([1,2,3,4])
puts first_positive([4,3,2,1])
puts first_positive([1,1,1,1])
puts first_positive([-1,-1,-3,3])
puts first_positive([2,3,4,5])