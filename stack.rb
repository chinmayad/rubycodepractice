CAPACITY=10
class Stack
  def initialize(size=CAPACITY)
    @stack = []
    @capacity = size
  end

  def push(val)
    return nil if (@stack.size == @capacity)
    @stack.push(val)
    true
  end

  def pop
    return nil if (@stack.size == 0)
    @stack.pop
  end

  def peek
    return nil if @stack.size == 0
    @stack[@stack.size-1]
  end

  def size
    @stack.size
  end
end
