=begin
Input:  words[] = {"baa", "abcd", "abca", "cab", "cad"}
Output: Order of characters is 'b', 'd', 'a', 'c'
Note that words are sorted and in the given language "baa"
comes before "abcd", therefore 'b' is before 'a' in output.
Similarly we can find other orders.

Input:  words[] = {"caa", "aaa", "aab"}
Output: Order of characters is 'c', 'a', 'b'
=end

def alienDic(words)
  letterHash = uniqueChars(words)
  makeGraph(letterHash, words)
  charOrder = []
  letterHash.each_value do |root|
    charOrder += topologicalSort(root)
  end
  return charOrder.reverse
end

def uniqueChars(words)
  tempHash = {}
  words.each do |w|
    w.each_char do |c|
      tempHash[c] = true if not tempHash[c]
    end
  end
  tempHash
end

def makeGraph(nodes, words)
  nodes.each_key do |letter|
    n = Node.new(letter)
    nodes[letter] = n
  end
  words.each_with_index do |w, index|
    next if index == 0
    compareAndEdge(nodes, words[index-1],w)
  end
  nodes
end

def compareAndEdge(nodeHash, w1, w2)
  for i in 0...w1.size do
    return if not w2[i]
    next if w1[i] == w2[i]
    nodeHash[w1[i]].children.push(nodeHash[w2[i]])
    break
  end
end

def topologicalSort(root)
  return [] if root == nil or root.visited == true
  root.visited = true
  result = []
  root.children.each do |child|
    result += topologicalSort(child) unless child.visited
  end
  result.push(root.val)
  return result
end

class Node
  def initialize(val,children=[])
    @val = val
    @children = children
    @visited = false
  end

  attr_accessor :val, :children, :visited
end

p alienDic(["baa", "abcd", "abca", "cab", "cad"])
p alienDic(["caa", "acq", "abb"])
p alienDic(["abc", "dcq", "dqh"])