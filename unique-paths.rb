=begin
A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

The robot can only move either down or right at any point in time. The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

How many possible unique paths are there?
=end


def unique_paths(m, n)
  memo = {}
  unique(0,0,m,n,memo)
end

def unique(r,c,m,n,memo)
  return memo[[r,c]] if memo[[r,c]]
  if r == m-1 && c == n-1
    memo[[r,c]] = 1
    return 1
  end
  sum = 0
  neighbors(r,c,m,n).each do |coord|
    sum += unique(coord[0], coord[1],m,n,memo)
  end
  memo[[r,c]] = sum
  sum
end

def neighbors(r,c,m,n)
  res = []
  res.push([r+1,c]) if r+1 < m
  res.push([r,c+1]) if c+1 < n
  res
end

puts unique_paths(3,7)
puts unique_paths(0,0)
puts unique_paths(1,1)