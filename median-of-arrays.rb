=begin
There are two sorted arrays nums1 and nums2 of size m and n respectively.

Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

Example 1:
nums1 = [1, 3]
nums2 = [2]

The median is 2.0
Example 2:
nums1 = [1, 2]
nums2 = [3, 4]
The median is (2 + 3)/2 = 2.5

Ex 3:
nums1 = [1,4,5] = 4
nums2 = [1,2,3] = 2
ans = 2.5

=end

def find_median_sorted_arrays(nums1, nums2)

end