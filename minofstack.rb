# Implement a stack where push , pop and min(of all values in stack) are all possible in O(1) time

class Node
  def initialize(val,index=nil)
    @val = val
    @min = index
  end
  attr_accessor :val, :min
end

class Stack
  def initialize(val=nil)
    if val == nil
      @store = []
      @min_index = nil
      @min_value = nil
    else
      node = Node.new(val)
      @store = [node]
      @min_index = 0
      @min_value = val
    end
  end

  def push(val)
    n = Node.new(val)
    if ((not @min_value) || val < @min_value)
      @min_value = val
      n.min = @min_index
      @min_index = @store.size
    end
    @store.push(n)
  end

  def pop()
    return nil if @store.size == 0
    n = @store.pop()
    if @store.size == 0
      @min_index = nil
      @min_value = nil
    end
    if (@store.size == @min_index)
      new_index = n.min
      @min_value = @store[new_index].val
      @min_index = new_index
    end
    n.val
  end

  def size
    @store.size
  end

  def clear
    @store.clear
    @min_index = nil
    @min_value = nil
  end

  attr_accessor :min_value
end

s = Stack.new()
puts s.min_value
s.push(5)
puts s.min_value
puts s.pop()
puts s.min_value
s.push(9)
s.push(6)
puts s.min_value
s.push(2)
puts s.min_value
s.push(4)
puts s.min_value
puts s.pop()
puts s.min_value
puts s.pop()
puts s.min_value
puts s.pop()
puts s.min_value()
puts s.pop()
puts s.min_value()
puts s.pop()
puts s.min_value