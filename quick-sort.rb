=begin
implement quick sort
=end


def quicksort(arr, low=0, high=arr.size-1)
  if low < high
    pivot = partition(arr,low,high)
    quicksort(arr,low,pivot-1)
    quicksort(arr,pivot+1,high)
  end
  arr
end

def partition(a, low, high)
  pivot = a[high]
  i = low-1
  for j in i+1...high do
    if a[j] < pivot
      i += 1
      swap(a,i,j)
    end
  end
  swap(a,i+1,high)
  return i+1
end

def swap(a,i,j)
  return if i == j
  temp = a[i]
  a[i] = a[j]
  a[j] = temp
end


p quicksort([10, 80, 30, 90, 40, 50, 70])