class DB
  # basic hash table that can be switched with any database
  def initialize
    @tiny_to_url = {}
    @url_to_tiny = {}
  end

  def hash_in_db(hash)
    return nil if not valid_hash(hash)
    if long = @tiny_to_url[hash]
      update_ts(hash)
      return long
    end
    log('queried for a tiny url that did not exist')
    return nil
  end

  def update_ts(url)
    # update timestamp of this row.
  end

  def url_in_db(url)
    if tiny = @url_to_tiny[url]
      # do nothing
    end
  end

  def valid_hash(hash)
    # check if url only has characters between a-z A-Z 0-9, 6 chars etc, not nil
  end
end