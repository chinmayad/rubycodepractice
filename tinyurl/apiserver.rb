require 'sinatra'
require_relative 'helper'
require_relative 'database'
set :port, 80

get '/t/:hash' do
  if (long_url = db.hash_in_db(params['hash']))
    return [long_urls, ERROR_CODE['HTTP_SUCCESS_CODE']]
  else
    log('cannot find a mapping url for this tiny url')
    return [ERROR_CODE['HTTP_PAGE_NOT_FOUND']]
  end
end

post '/t/:url' do
  if not valid_url(params['url'])
    log('invalid url provided')
    return [ERROR_CODE['HTTP_ERROR']]
  end

  if tiny = db.url_in_db(params['url'])
    return [[tiny, ERROR_CODE['HTTP_SUCCESS_CODE']]]
  end
end





