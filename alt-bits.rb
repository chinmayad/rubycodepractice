=begin
Given a positive integer, check whether it has alternating bits or not.

Example 1:
Input: 5
Output: True
Explanation:
The binary representation of 5 is: 101
Example 2:
Input: 7
Output: False
Explanation:
The binary representation of 7 is: 111
=end

def has_alternating_bits(n)
  return false if n == nil
  while n != 0 do
    flag1 = n & 1
    n = n >> 1
    flag2 = n & 1
    return false if flag1 == flag2
  end
  return true
end


p has_alternating_bits(5)
p has_alternating_bits(7)
p has_alternating_bits(255)
p has_alternating_bits(256)
p has_alternating_bits(4)