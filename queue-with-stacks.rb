require_relative 'stack'

class MyQueue
  def initialize
    @s1 = Stack.new
    @s2 = Stack.new
  end

  def enq(val)
    @s1.push(val)
  end

  def deq
    val = @s2.pop
    if val == nil
      resync
      @s2.pop
    else
      val
    end
  end

  def resync
    while (val = @s1.pop)
      @s2.push(val)
    end
  end
end


q = MyQueue.new
puts q.deq
q.enq(33)
q.enq(32)
q.enq(31)
puts q.deq
q.enq(44)
q.enq(30)
q.enq(55)
puts q.deq
puts q.deq
puts q.deq
puts q.deq
puts q.deq