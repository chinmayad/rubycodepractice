class Node
  def initialize(val)
    @val = val
    @left = nil
    @right = nil
  end

  attr_accessor :left, :right , :val
end

class DoublyLinkedList
  def initialize(val=nil)
    @head = val ? Node.new(val) : nil
    @size = (val == nil) ? 0 : 1
  end

  def insert(val)
    node = Node.new(val)
    node.right = @head
    @head.left = node if @head
    @head = node
    @size += 1
  end


  def delete(val)
    current = @head
    while (current != nil)
      if (current.val == val)
        ndelete(current)
        return true
      end
      current = current.right
    end
    return false
  end

  def ndelete(node)
    node.right.left = node.left if node.right
    node.left.right = node.right if node.left
    @head = node.right if @head == node
    @size -= 1
  end

  attr_reader :head, :size
end
