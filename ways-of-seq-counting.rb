=begin
given a number, find number of ways of sequentially(>=2) adding up to that number.
Ex : 15
num of ways = 3
1+2+3+4+5
4+5+6
7+8
=end

def consecutive(num)
  basePtr = 0
  cntr = 0
  i = 0
  while i < num do
    if sigma(i) - sigma(basePtr) == num
      cntr += 1
      i += 1
    elsif sigma(i) - sigma(basePtr) > num
      basePtr += 1
    else
      i += 1
    end
  end
  cntr
end

def sigma(num)
  (num * (num + 1))/2
end

p consecutive(15)