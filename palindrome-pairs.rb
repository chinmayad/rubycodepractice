=begin
Given a list of unique words, find all pairs of distinct indices (i, j) in the given list, so that the concatenation of the two words, i.e. words[i] + words[j] is a palindrome.

Example 1:
Given words = ["bat", "tab", "cat"]
Return [[0, 1], [1, 0]]
The palindromes are ["battab", "tabbat"]
Example 2:
Given words = ["abcd", "dcba", "lls", "s", "sssll"]
Return [[0, 1], [1, 0], [3, 2], [2, 4]]
The palindromes are ["dcbaabcd", "abcddcba", "slls", "llssssll"]
=end

# @param {String[]} words
# @return {Integer[][]}
def palindrome_pairs(words)
  wordHash = {}
  words.each_with_index do |word, index|
    wordHash[word] = index
  end
  res = []
  words.each_with_index do |word, index|
    revWord = word.reverse
    for i in 0...revWord.length
      w = revWord[i...revWord.length]
      if ((ind = wordHash[w]) && isPalindrome(word + w))
        res.push([index,ind]) unless ind == index
      end
      w = revWord[0...revWord.length-i-1]
      if ((ind = wordHash[w]) && isPalindrome(w + word))
        res.push([ind,index]) unless ind == index
      end
    end

    if (ind = wordHash['']) && isPalindrome(word)
      res.push([index, ind]) unless index == ind
    end
  end
  res
end

def isPalindrome(w)
  len = w.length - 1
  for i in 0..len/2 do
    return false if w[i] != w[len-i]
  end
  return true
end


p palindrome_pairs(["bat", "tab", "cat"])
p palindrome_pairs(["abcd", "dcba", "lls", "s", "sssll"])
p palindrome_pairs([ "ss", "ls", "s", "sss"])
p palindrome_pairs(['a', ''])
p palindrome_pairs(["a","b","c","ab","ac","aa"])