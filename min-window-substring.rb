=begin
https://leetcode.com/problems/minimum-window-substring/description/
Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).

For example,
S = "ADOBECODEBANC"
T = "ABC"
Minimum window is "BANC".

Note:
If there is no such window in S that covers all characters in T, return the empty string "".

If there are multiple such windows, you are guaranteed that there will always be only one unique minimum window in S.
=end



# make a hash of T with count as value . Call this hash H. Let counter == numElements in T
# iterate through S, for each char
#   find all elements in T in S. if S ends return ''. Decrement counter and H[c] as we go.
#   if all elements are found, counter will be 0. let the ptr be called last
#       save the starting character and length in S unless a lower length is S has already been found
#       find the first matching character and increment H[c], let this ptr be called first.
#       keep moving the last ptr till the same element is found ahead. Save min length if < cur length
#   exit if last > len of S

def minWindowStr(str, pattern)
  cMap = {}
  pattern.each_char do |c|
    if cMap[c]
      cMap[c] += 1
    else
      cMap[c] = 1
    end
  end
  # S = "ADOBECODEBANC"
  # T = "ABC"
  pCounter = pattern.size
  last = 0
  minStart = 0
  first = 0
  minLength = str.length + 1
  while last < str.length do
    c = str[last]
    if cMap[c]
      pCounter -= 1 if cMap[c] > 0
      cMap[c] -= 1
    end
    last += 1
    while pCounter == 0 do
      # we have found a matching substring
      len = last - first - 1
      if len < minLength
        minLength = len
        minStart = first
      end
      c = str[first]
      if cMap[c]
        cMap[c] += 1
        pCounter += 1 if cMap[c] > 0
      end
      first += 1
    end
  end
  if minLength == str.length + 1
    return ''
  else
    return str[minStart..(minStart+minLength)]
  end
end

s = "ADOBECODEBANC"
t = "ABC"
puts minWindowStr(s,t)




def minWindow(s,t)
  tmap = Array.new(128) {|x| 0}
  t.each_char do |c|
    tmap[c.ord] += 1
  end
  len = s.size
  counter = t.size
  start = 0
  finish = 0
  minLength = 1000000
  while (finish < len) do
    if tmap[s[finish].ord] > 0
      counter -= 1
    end
    tmap[s[finish].ord] -= 1
    finish += 1
    while counter == 0 do
      if ((finish - start) < minLength)
        minLength = finish - start
        minstart = start
      end
      tmap[s[start].ord] += 1
      if tmap[s[start].ord] > 0
        counter += 1
      end
      start += 1
    end
  end
  return '' if minLength == 1000000
  return s[minstart...minstart+minLength]
end

S = "ADOBECODEBANC"
T = "ABC"
#puts minWindow(S,T)
