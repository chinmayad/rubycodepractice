class Node
  def initialize(val)
    @val = val
    @nex = nil
  end

  attr_accessor :nex, :val
end

class SinglyLinkedList
  def initialize(val=nil)
    @head = val ? Node.new(val) : nil
    @size = (val == nil) ? 0 : 1
  end

  def insert(val)
    node = Node.new(val)
    insert_node(node)
  end

  def insert_node(node)
    node.nex = @head
    @head = node
    @size += 1
  end

  def delete(val)
    current = @head
    previous = @head
    while (current != nil)
      if (current.val == val)
        previous.nex = current.nex
        @size -= 1
        @head = current.nex if @head == current
        return true
      end
      previous = current
      current = current.nex
    end
    return false
  end

  def find(n)
    current = @head
    while (current != nil)
      return true if (current == n)
      current = current.nex
    end
    return false
  end

  attr_reader :head, :size
end

