=begin
#Implement regular expression matching with support for '.' and '*'.

'.' Matches any single character.
    '*' Matches zero or more of the preceding element.

    The matching should cover the entire input string (not partial).

        The function prototype should be:
                                          bool is_match(const char *s, const char *p)

Some examples:
is_match("aa","a") → false
is_match("aa","aa") → true
is_match("aaa","aa") → false
is_match("aa", "a*") → true
is_match("aa", ".*") → true
is_match("ab", ".*") → true
is_match("aab", "c*a*b") → true
=end

# @param {String} s
# @param {String} p
# @return {Boolean}


def is_match(s, p)
  hash = {}
  return isMatch(s, p)
end

def pValue(p)
  i = 0
  while i < p.size do
    if p[i+1] && p[i+1] != '*'
      return false
    end
    i = i + 2
  end
  return (p[p.size-1] == '*')
end

def isMatch(s, p)
  if p == ''
    return  (s == '')
  end
  if s == ''
    return pValue(p)
  end
  match = ((p[0] == s[0]) || (p[0] == '.' && s[0]))
  if p[1] && (p[1] == '*')
    return ( (match && isMatch(s[1..-1], p[0..-1])) || (isMatch(s[0..-1], p[2..-1])) )
  end

  return (match && isMatch(s[1..-1], p[1..-1]) )
end

time = Time.now
puts '1fail' if true == is_match("aaaaaaaaaaaaab" ,"a*a*a*a*a*a*a*a*a*a*c")
puts Time.now - time


puts '************************'
puts '2fail!' if true != is_match("aa","aa")
puts '3fail!' if false != is_match("aaa","aa")
puts '4fail!' if true != is_match("aa", "a*")
puts '5fail!' if true != is_match("aa", ".*")
puts '6fail!' if true != is_match("ab", ".*")
puts '7fail!' if true != is_match("aab", "c*a*b")

puts '8fail!' if false != is_match("aa","*a")
puts '9fail!' if true != is_match("aa","aa")
puts '10fail!' if false != is_match("aa",".aa")
puts '11fail!' if true != is_match("aaa","aa.")
puts '12fail!' if false != is_match("aa", "*")

puts '13fail!' if false != is_match("aa", "a*bc")
puts '14fail!' if true != is_match("ab", "ab.*")
puts '15fail!' if false != is_match("aab", "aabc")