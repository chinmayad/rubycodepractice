=begin

Maximum product subset with negative and positive integers
http://www.geeksforgeeks.org/maximum-product-subarray/

=end

# nums = [4,-1, 3 , 5 , -9]
# algo

#

def max(a,b,c)
  ab = a > b ? a : b
  ab > c ? ab : c
end

def min(a,b,c)
  ab = a < b ? a : b
  ab < c ? ab : c
end

def maxProduct(nums)
  max_p = 0
  max_n = 0
  max_neutral = -1000000
  return nil if nums.size == 0
  nums.each do |n|
    case
      when n < 0
      if max_n == 0
        max_n = (max_p == 0) ? n : max_p * n
      else
        max_pTemp = max_n * n
        max_n = min(max_n, n, max_p * n)
        max_p = max_pTemp if max_pTemp > max_p
      end
      when n > 0
      if max_p == 0
        max_p = n
      else
        max_p = max_p * n
      end
      max_n = max_n * n
      when n == 0
        max_neutral = 0
    end
  end
  if max_p == 0
    max(max_n, max_neutral, max_neutral)
  else
    max(max_p, max_n, max_neutral)
  end
end

puts maxProduct([6, -3, -10, 0, 2])
puts maxProduct([-1, -3, -10, 0, 60])
puts maxProduct([-2, -3, 0, -2, -40])
puts maxProduct([-5])
puts maxProduct([0])
puts maxProduct([-3,0])
