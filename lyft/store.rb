require_relative 'versioner'
require_relative 'entry'

class Store
  def initialize
    @store = {}   # key to (array of entries) map
    @versioner = Versioner.new
  end

  def store(key, value)
    if @store[key]
      arrOfEntries = @store[key]
    else
      arrOfEntries = []
      @store[key] = arrOfEntries
    end
    version = @versioner.getNextVersion
    e = Entry.new(version, value)
    arrOfEntries.push(e)
    version
  end

  # returns value or nil
  def readLatest(key)
    return nil if not @store[key]
    arrOfEntries = @store[key]
    arrOfEntries.last.value
  end

  # returns value or nil
  def read(key,version)
    return nil if not @store[key]
    arrOfEntries = @store[key]
    return readLatest(key) if version > arrOfEntries.last.version # if version greater than latest version,return latest
    return nil if version < 1
    findBestValue(arrOfEntries,version)  # can return nil
  end

  def findBestValue(arr, version, low = 0, high = arr.size-1)
    return nil if low > high
    med = (low + high)/2
    return arr[med].value if (arr[med].version == version)
    if med == 0
      temp = 0
    else
      temp = med - 1
    end
    return arr[temp].value if low == high   # return best lower version if array is searched
    if version > arr[med].version
      return findBestValue(arr, version, med+1, high)
    else
      return findBestValue(arr, version, low, med-1)
    end
  end
end
