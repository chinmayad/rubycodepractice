require_relative 'store'

module KeyStore
  class Runner
    def initialize(filename)
      @filename = filename
      @storage = Store.new
    end

    def runner
    f = File.new(@filename)
    # input validation TODO
    File.readlines(f).each do |line|
      cmd = line.split
      key = cmd[1].chomp
      # input validation TODO
      case cmd[0]
        when 'PUT'
          value = cmd[2].chomp
          version = @storage.store(key,value)
          puts "PUT(##{version}) #{key} = #{value}"
        when 'GET'
          if cmd[2]
           version = cmd[2].chomp.to_i
           value = @storage.read(key, version)
           value = 'nil' if value == nil
           puts "GET #{key}(#{version}) = #{value}"
          else
            value = @storage.readLatest(key)
            value = 'nil' if value == nil
            puts "GET #{key} = #{value}"
          end
        end
      end
    end
  end
end

r = KeyStore::Runner.new(ARGV[0])
r.runner

