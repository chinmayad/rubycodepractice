class Entry
  def initialize(version, value)
    @version = version
    @value = value
  end

  attr_accessor :value, :version
end