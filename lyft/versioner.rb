class Versioner
  def initialize
    @version = 0
  end

  def getNextVersion
    @version += 1
    return @version
  end
  # works currently only for single threaded execution
end