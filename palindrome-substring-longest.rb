# Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.


def getPalCentredAt(index, str)
  variance = 1
  len = str.length - 1
  tolerance = index < len-index ? index : len - index
  while (variance <= tolerance) do
    return (variance * 2 - 1) if str[index-variance] != str[index+variance]
    variance += 1
  end
  return (variance * 2 - 1)
end


def getPalAfter(index, str)
  variance = 0
  len = str.length - 1
  tolerance = index < len-index ? index : len - index - 1
  while (variance <= tolerance) do
    return (variance * 2) if str[index-variance] != str[index+variance+1]
    variance += 1
  end
  return (variance * 2)
end


def palindrome(str)
  return nil if str == nil || str.length == 0
  len = str.length
  index = 0
  cur_longest_substring = 0
  while (index < len) do
    l_centre = getPalCentredAt(index, str)
    l_after  = getPalAfter(index, str)
    if (l_centre > cur_longest_substring)
      cur_longest_substring = l_centre
      pal_index = index
    end
    if l_after > cur_longest_substring
      cur_longest_substring = l_after
      pal_index = index + 0.5
    end
    break if ((len - index) < (cur_longest_substring.to_f / 2))
    index += 1
  end
  if (pal_index != pal_index.floor)
    return str[(pal_index.floor-((cur_longest_substring/2)-1))..(pal_index.floor + (cur_longest_substring/2))]
  else
    return str[(pal_index - (cur_longest_substring - 1)/2)..(pal_index + (cur_longest_substring - 1)/2)]
  end
end

puts palindrome("")
puts palindrome("abballabbsa")