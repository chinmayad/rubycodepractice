=begin
https://www.interviewcake.com/question/python/cake-thief?

cake_tuples = [(7, 160), (3, 90), (2, 15)]
capacity    = 20

max_duffel_bag_value(cake_tuples, capacity)
# returns 555 (6 of the middle type of cake and 1 of the last type of cake)
=end


def max_duffle(cakes, capacity)
  return 0 if capacity == 0
  max = 0
  cakes.each do |cake|
    next if cake.wt > capacity
    tmp = cake.price + max_duffle(cakes, capacity - cake.wt)
    max = tmp if max < tmp
  end
  max
end

# 2,15
# 5
# (c, 3, 15)
# (c, 1, 30)




class Cakes
  def initialize(wt, price)
    @wt = wt
    @price = price
  end

  attr_reader :wt, :price
end

c = [ Cakes.new(2,15)]
puts max_duffle(c, 5)

c = [Cakes.new(7,160), Cakes.new(3,90), Cakes.new(2,15)]
puts max_duffle(c, 20)
