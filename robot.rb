# robot can only go right or down. Find a way to the end of the maze. maze has 1 for prohibited cubes.
class MazeRunner
  def initialize(maze, r, c)
    @maze = maze
    @path = []
    @rows = r
    @cols = c
  end

  def valid(r,c)
    @maze[r][c] == 0 ? true : false
  end

  def getPathToOrigin(row = (@rows - 1),col = (@cols - 1))
    return false if @maze == nil
    return false if (row < 0 || col < 0)
    if (row == 0 && col == 0)
      return true
    else
      if valid(row, col-1) && getPathToOrigin(row, col-1)
        @path.push([row, col-1])
        return true
      elsif valid(row-1, col) && getPathToOrigin(row-1, col)
        @path.push([row-1, col])
        return true
      else
        return false
      end
    end
  end
  attr_reader :path
end
maze = [[0,0,0], [0,1,0], [1,1,0]]
m = MazeRunner.new(maze, 3,3)
puts m.getPathToOrigin
p m.path








#c=0 1 2  r
#  0 0 0  0
#  0 1 0  1
#  1 1 0  2