=begin
Given a list of non-negative numbers and a target integer k, write a function to check if the array
 has a continuous subarray of size at least 2 that sums up to the multiple of k, that is, sums up
to n*k where n is also an integer.
=end

a = [6,3,1,1]
k = 11

def summer(arr,k, num=k)
  return false if not arr
  return true if arr[0] == k
  arr.each do |e|
    newk = nextPositive(k,e,num)
    return true if summer(arr-[e],newk, num)
  end
  false
end

def nextPositive(k,e,num)
  i = 0
  while true do
    res = (k - e + i * num)
    return res if res > 0
    i += 1
  end
end

puts summer(a,k)

# 3,2,6,5, 10
# 2,6,5    7
# 6,5      5
#
