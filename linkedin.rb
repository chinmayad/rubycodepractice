=begin

Given a binary search tree and a target value, find k values in the binary search tree (BST) that are closest to the target.

Given the following Binary Search Tree:
                10
               /  \
              5    20
             /\    /\
            1  6  15 30
                \
                 7
Given target=8, and k=3, it should return 6,7,10


=end



class Node
  def initialize(val, left=nil, right=nil)
    @val = val
    @left = left
    @right = right
  end
  attr_accessor :val, :left, :right
end

def getClosestK(root, target,k)
  q = []
  closestToTarget(root, target,k, q)
  q
end

# q = []
# root = 1 , [1]
# root == 5, [1,5]
# root == 6, [1,5,6]
# root == 7, [5,6,7]
# root == 10, # (5 - 8) > (10 - 8)  [6,7], [6,7,10]
# root == 15, # (6-8) > (15-8)

def closestToTarget(root, target, k, q)
  return if root == nil
  closestToTarget(root.left, target, k, q)

  if q.size < k
    q.push(root.val)
  else
    if (q[0] - target).abs > (root.val - target).abs
      q.shift # q.dequeue
      q.push(root.val)
    else
      return
    end
  end
  closestToTarget(root.right, target, k, q)
end

mytree = Node.new(10, Node.new(5, Node.new(1), Node.new(6)), Node.new(20, Node.new(15), Node.new(30, Node.new(29),
                                                                                                 Node.new(40))));
p getClosestK(mytree, 26, 3)

def sqrt(num, acc = 0.5)
  low = 1.0
  high = num.to_f
  while ((high-low).abs > acc) do
    high = (low+high)/2
    low = num/high  # low * high = num
  end
  low
end

puts sqrt(12)






