=begin
Flip Binary Tree
4.3
Given a binary tree, the task is to flip the binary tree towards right direction that is clockwise.
 See below examples to see the transformation.

In the flip operation, left most node becomes the root of
flipped tree and its parent become its right child and the right sibling become its
 left child and same should be done for all left most nodes recursively.

http://www.geeksforgeeks.org/flip-binary-tree/
=end


class Node
  def initialize(value, left=nil, right=nil)
    @value = value
    @left = left
    @right = right
  end
  attr_accessor :left, :right, :value
end


def flipBTree(root)
  return root if root == nil
  return root if root.left == nil and root.right == nil
  newRoot = flipBTree(root.left)
  root.left.left = root.right
  root.left.right = root
  root.left = nil
  root.right = nil
  return newRoot
end




mytree = Node.new(1, Node.new(2), Node.new(3, Node.new(4), Node.new(5)))
h =  flipBTree(mytree)
puts 'hi'


=begin
        7
     8          15
13      44  3         14
                  1         17

              1
           2     3
               4    5
=end

