=begin
k largest(or smallest) elements in an array

Question: Write an efficient program for printing k largest elements in an array. Elements in array can be in any order.

For example, if given array is [1, 23, 12, 9, 30, 2, 50] and you are asked for the largest 3 elements i.e., k = 3
\then your program should print 50, 30 and 23.

http://www.geeksforgeeks.org/k-largestor-smallest-elements-in-an-array/
=end


def kLargest(nums, k, low=0, high=nums.size-1)
  puts 'hi'
  if low < high
    pivot = partition(nums, low,high)
    return nums[0...k] if pivot == k || pivot == k-1
    if pivot > k
      kLargest(nums, k, low, pivot-1)
    else
      kLargest(nums, k, pivot+1, high)
    end
  end
  nums[0...k]
end

def partition(nums, low, high)
  pivot = nums[high]
  i = low-1
  for j in i+1...high do
    if nums[j] < pivot
      i += 1
      swap(nums, i, j)
    end
  end
  swap(nums, i+1, high)
  i+1
end


def swap(arr, i, j)
  return if i == j
  temp = arr[i]
  arr[i] = arr[j]
  arr[j] = temp
end

n = [10, 90, 40, 70, 70, 20, 10]
kLargest(n, 5)
puts n