=begin
Given a flight itinerary consisting of starting city, destination city, and ticket price (2d list) -
find the optimal price flight path to get from start to destination. (A variation of Dynamic Programming Shortest Path)
Ex :
max_num_flights = 3
starting city = 1
destination = 3
         0   1   2   3
tp = 0 [ 0,  -, 15, 30]
     1 [ 15, 0, 60, 55]
     2 [ 20, 5, 0,  10]
     3 [ 15, -, 25, 0 ]

soln = 1->0->2->3
        15 15 10
cost = 40
=end


class Node
  def initialize(id)
    @id = id
    @visited = false
    @from = nil
    @cost = 1000000
  end
  attr_accessor :id, :visited, :from, :cost
end
# node.cost = cost from src city
# node.from = the node from which node.cost exists
def cheapFlights(tp,src,dest,max)
  # initialize nodes for each city
  cities = {}
  tp.each_with_index do |val,id|
    n = Node.new(id)
    cities[id] = n
  end
  cities[src].cost = 0
  # initialize nodes.cost = MAX_INTEGER
  # initialize nodes.visited = false
  cheapest(tp,src,dest,max, cities)
  path = findCost(cities[dest],cities[src])
  return path + [src], cities[dest].cost
end

def findCost(dest,src)
  path = []
  while dest != src
    path.push(dest.id)
    dest = dest.from
  end
  return path
end

def neighbors(tp,id,cities)
  nei = []
  tp[id].each_with_index do |n, index|
    nei.push(cities[index]) if n.is_a?(Integer)
  end
  nei
end

def cheapest(tp,src,dest,max, cities)
  q = []
  srcNode = cities[src]
  q.push(srcNode)
  q.push(0)
  while srcNode = q.shift
    if srcNode.is_a? (Integer)
      depth = srcNode
      return if depth > max
      q.push(depth+1)
      next
    end
    srcNode.visited = true
    src = srcNode.id
    neighbors(tp, src, cities).each do |n|
      destNode = n
      dest = destNode.id
      if tp[src][dest].is_a?(Integer) && (srcNode.cost + tp[src][dest] < destNode.cost)
        destNode.from = srcNode
        destNode.cost = srcNode.cost + tp[src][dest]
      end
      q.push(n) unless n.visited
    end
  end
end

tp = [[ 0,  '-', 15, 30],
      [ 15, 0, 60, 55],
      [ 20, 5, 0, 10],
      [ 15, '-', 25, 0 ]]


#puts cheapFlights(tp, 1,3, 3)






def flightChecker(tp, src, dest, max)
  cities = []
  tp.each_with_index do |v, index|
    n = Node.new(index)
    cities[index] = n
  end
  cities[src].cost = 0
  queue = []
  queue.push(cities[src])
  queue.push(1)
  cities[src].visited = true
  while srcNode = queue.shift
    if srcNode.is_a?(Integer)
      break if srcNode >= max
      queue.push(srcNode+1)
      next
    end
    s = srcNode.id
    neighbors(tp,srcNode.id, cities).each do |destNode|
      if tp[s][destNode.id].is_a?(Integer) && (srcNode.cost + tp[s][destNode.id] < destNode.cost)
        destNode.cost = srcNode.cost + tp[s][destNode.id]
        destNode.from = srcNode
      end
      queue.push(destNode) unless destNode.visited
      destNode.visited = true
    end
  end
  path = findCost(cities[dest],cities[src])
  res = (path + [src]).reverse
  return res, cities[dest].cost
end


p flightChecker(tp, 1,3, 2)
