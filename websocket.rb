require 'socket'
require 'digest'


def gen_return_key(key)
  magic_number = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
  Digest::SHA1.base64digest(key + magic_number)
end

def get_key(req)
  web_key = req.match(/Sec-WebSocket-Key: (\S+)/)
  raise 'Not a secure websocket connection' unless web_key
  web_key[1]
end

def gen_response(key)
  "HTTP/1.1 101 Switching Protocols
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Accept: #{key}"
end


def start_server
  t = TCPServer.new(2345)
  while (conn = t.accept) do
    puts 'Incoming request'
    http_req = ''
    while ((line = conn.gets) && (line != "\r\n")) do
      http_req += line
    end
    puts http_req
    key = get_key(http_req)
    hash = gen_return_key(key)
    res = gen_response(hash)
    conn.write(res)
    puts 'handshake complete'
    sleep 100
    #conn.close
  end
end

start_server