# given matrix M x N, where each roq and column are sorted. Find an element


# a = [ 1   3   5   8   9  ]
#     [ 2   4   6   9   15 ]
#     [ 8   8   8   13  15 ]
#
#
# Algo

# 1) Do binary search on first row to find two elements between which the element lies. Let the latter
# column be K
# 2) Now the matrix for search is reduced to (2 to M rows, 1 to K-1 cols)  OR (2 to M rows, K to N cols), whichever
# is smaller.  Repeat step 1 on this subset matrix. Keep doing this until u land on the element.
