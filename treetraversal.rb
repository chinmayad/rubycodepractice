# Traverse a binary tree in order, post order and pre order and print the node values.


class Node
  def initialize(value, left=nil, right=nil)
    @value = value
    @left = left
    @right = right
  end
  attr_accessor :left, :right, :value
end

def traverse_inorder(root)
  traverse_inorder(root.left) if root.left
  puts root.value
  traverse_inorder(root.right) if root.right
end

def traverse_preorder(root)
  puts root.value
  traverse_preorder(root.left) if root.left
  traverse_preorder(root.right) if root.right
end

def traverse_postorder(root)
  traverse_postorder(root.left) if root.left
  traverse_postorder(root.right) if root.right
  puts root.value
end

mytree = Node.new(7, Node.new(8, Node.new(13), Node.new(44)), Node.new(15, Node.new(3), Node.new(14, Node.new(1),
                                                                                                 Node.new(17))));

puts '****************************'
traverse_inorder(mytree)
puts '****************************'
traverse_preorder(mytree)
puts '****************************'
traverse_postorder(mytree)
puts '****************************'