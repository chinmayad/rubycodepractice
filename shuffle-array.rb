def shuffler(a)
  res = a.dup
  i =res.size - 1
  while i > 0 do
    j = rand(i)
    swap(res,i,j)
    i -= 1
  end
  res
end

def swap(arr,a,b)
  temp = arr[a]
  arr[a] = arr[b]
  arr[b] = temp
end

p shuffler([1,2,3,4,5])