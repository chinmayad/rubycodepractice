=begin
Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).

    You may assume that the intervals were initially sorted according to their start times.
        Example 1:
    Given intervals [1,3],[6,9], insert and merge [2,5] in as [1,5],[6,9].

    Example 2:
    Given [1,2],[3,5],[6,7],[8,10],[12,16], insert and merge [4,9] in as [1,2],[3,10],[12,16].


    [1 3] [7 9] insert [10 30]
[5,7] [11,13] insewrt [1,2]
=end


# algo
# series : array of arrays
# interval : array
# return : list of intervals
def merge_interval(series, int)
# TODO:  input validation
  i = 0
  result = []
  while i < series.size do
    s = series[i]
    i = i+1
    if int[0] > s[1]
      # push series to result
      result.push(s)
      # move to next series
      next
    end
    if int[1] < s[0]
      # return interval + series
      return (interval + series)
    end
    if int[0] > s[0] && int[1] < s[1]
      return series
    end
    if int[0] > s[0] && int[1] > s[1]
      flag = 1
      while i < series.size do
        s2 = series[i]
        i = i+1
        if int[1] < s2[0]
          result.push([s[0], int[1]])
          result.push(s2)
          flag = 0

          break;
        elsif int[1] > s2[0] && int[1] < s2[1]
          result.push([s[0], s2[1]])
          flag = 0

          break;
        else
          # nop
        end

      end
      if i == series.size && flag == 1
        result.push([s[0], int[1]])
        return result
      end
      while i < series.size
        result.push(series[i])
        i = i + 1
      end
    end
  end

  return result
end

p merge_interval([[1,2],[3,5],[6,7],[8,10],[12,16]], [4,9])
p merge_interval([[1,3],[6,9]], [2,5])