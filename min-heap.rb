include Math
class MinHeap
  def initialize(arr)
    @heap = arr
    makeHeap
  end

  def left(parent)
    2*parent + 1
  end

  def right(parent)
    2*parent + 2
  end

  def valueAt(index)
    @heap[index]
  end

  def minify(index)
    return index if left(index) >= @heap.size
    left = valueAt(left(index))
    centre = valueAt(index)
    min = left < centre ? left(index) : index
    return min if right(index) >= @heap.size
    right = valueAt(right(index))
    valueAt(min) < right ? min : right(index)
  end

  def swap(a,b)
    return nil if a == b
    temp = @heap[a]
    @heap[a] = @heap[b]
    @heap[b] = temp
  end

  def heapify(index)
    minIndex = minify(index)
    swap(minIndex, index)
  end

  def findDepth
    size = @heap.size
    fl = log(size,2).floor
    return fl
  end

  def elementsAtDepth(depth)
    return [0] if depth == 0
    startIndex = (2 ** depth) - 1
    endIndex = (2 ** (depth + 1)) - 2
    endIndex = @heap.size - 1 if endIndex > (@heap.size - 1)
    (startIndex..endIndex).to_a
  end

  def makeHeap
    endDepth = 0
    while (endDepth <= findDepth - 1) do
      dep = findDepth - 1
      while (dep >= endDepth) do
        elementsAtDepth(dep).each do |index|
          heapify(index)
        end
        dep = dep - 1
      end
      endDepth = endDepth + 1
    end
  end

  def fixHeapFromAbove(index=0)
    minIndex = minify(index)
    return if minIndex == index
    swap(minIndex, index)
    fixHeapFromAbove(minIndex)
  end

  def getParent(index)
    ((index-1)/2).floor
  end

  def fixHeapFromBelow(index=@heap.size-1)
    parent = getParent(index)
    fixHeapFromBelow(parent) if heapify(parent) != nil
  end

  def getMin
    swap(0 , @heap.size - 1)
    val = @heap.pop
    fixHeapFromAbove
    val
  end

  def insert(val)
    @heap.push(val)
    fixHeapFromBelow
  end
end


arr1 = [15,14,13,12,11,8,7,6,5,4,7,17,1]
heaper = MinHeap.new(arr1)
puts heaper.getMin
puts heaper.getMin
puts heaper.getMin
heaper.insert(9)
puts heaper.getMin