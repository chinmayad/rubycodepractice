=begin
Given two words (beginWord and endWord), and a dictionary's word list, find all shortest transformation sequence(s) from beginWord to endWord, such that:

Only one letter can be changed at a time
Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
For example,

Given:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log","cog"]
Return
  [
    ["hit","hot","dot","dog","cog"],
    ["hit","hot","lot","log","cog"]
  ]
Note:
Return an empty list if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.
=end



def find(b,e, dic, curMin = 10000, depth = 0)
   return [] if depth > curMin
   return [[e]] if b == e
   temp = []
   dic.each do |d|
      if strcmp(b,d) == 1
        t = find(d,e,dic-[b], curMin, depth+1)
        t.each do |e|
          e.unshift(b)
        end
        temp += t
      end
   end
   temp.each do |t|
     if t.size + depth <= curMin
       curMin = t.size + depth
     end
   end
   temp.select {|r| r.size <= curMin}
end

def find_ladders(bword, eword, dic)
  return [] if not dic.include?(eword)
  # input validation , return if bword == eword or if strcmp(bword, eword) = 1char
  res = optimize(bword, eword, dic-[eword])
  res.each do |r|
    r.unshift(bword)
  end
 min_length_filter(res)
end

def optimize(bword, eword, dic)

  set_res = []
  dic.each do |word|
    res = nil
    if (1 == strcmp(word, bword))
      if 1 == strcmp(word, eword)
        return [[word, eword]]
      end
      result = optimize(word, eword, dic - [word])
      if result != nil
        result += res if res
        result.each do |r|
          r.unshift(word)
        end
        set_res += result
      end
    end
  end
  #set_res = min_length_filter(set_res) if set_res
  return set_res
end

def min_length_filter(res)
  min = 100000
  newres = []
  if res != nil
    res.each do |e|
      min = e.size < min ? e.size : min
    end
    res.each_with_index do |e, index|
      if e.size == min
        newres.push(e)
      end
    end
  end
  newres
end


# return num characters diff bw 1 and 2
# return 0 if words are the same
# return 1 if words have 1 char diff
# return 2 if words have more than 1 char diff
def strcmp(word1, word2)
  return 0 if word1 == word2
  count  = 0
  for index in 0..word1.size do
    if word1[index] != word2[index]
      count = count + 1
      return 2 if count > 1
    end
  end
  return count
end

beginWord = "a"
endWord = "c"
wordList = ["a","b","c"]

p find_ladders(beginWord, endWord, wordList)

beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log","cog"]

p find(beginWord, endWord, wordList)