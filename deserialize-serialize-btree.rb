class Node
  def initialize(val, left = nil, right = nil)
    return nil if val == 'nil'
    @val = val
    @left = left
    @right = right
  end

  def to_s
    @val.to_s
  end

  attr_accessor :val, :left, :right
end

def serialize(root)
  return 'nil' if root == nil
  str = root.to_s
  str += ',' + serialize(root.left)
  str += ',' + serialize(root.right)
  str
end


def deserialize(str)
  list = str.split(',')
  deserializer(list)
end

def deserializer(list)
  if list[0] == 'nil'
    list.shift
    return nil
  end
  root = Node.new(list.shift)
  root.left = deserializer(list)
  root.right = deserializer(list)
  root
end

btree = Node.new('3', Node.new(5), Node.new(7, Node.new(1), Node.new(9)))
str = serialize(btree)
puts str
r = deserialize(str)
puts r.val