# Given a linked list, remove all duplicates


require_relative 'dlinkedlist'

ll = DoublyLinkedList.new()
p ll.insert(4)
p ll.insert(7)
p ll.insert(11)
p ll.insert(0)
p ll.insert(11)
p ll.insert(4)
p ll.insert(11)

def remove_dups(ll)
  current = ll.head
  valstore = {}
  while (current != nil)
    if valstore[current.val]
      ll.ndelete(current)
    else
      valstore[current.val] = 0
    end
    current = current.right
  end
end

remove_dups(ll)
puts 'completed test'