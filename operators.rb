=begin
Expression operators? Add signs to a string to form target (All 4 signs +, -, *, /)
https://leetcode.com/problems/expression-add-operators/description/
=end

def strWayFinder(str,target)
  return '' if str == ''
  strOperator(str[1..-1],target,str[0].to_i, str[0].to_i, str[0])
end

def strOperator(str, target, prev, eval, res)
  return res if (str == '' && target == eval)
  return '' if (str == '' && target != eval)
  r = []
  for i in 0...str.length
    n1 = str[0..i].to_i
    tempOut = strOperator(str[i+1..-1], target, n1,  n1 + eval, res  + '+' + str[0..i])
    r << tempOut if tempOut != ''
    tempOut = strOperator(str[i+1..-1], target, n1, eval - n1, res  + '-' + str[0..i])
    r << tempOut if tempOut != ''
    tempOut = strOperator(str[i+1..-1], target, prev*n1, eval - prev + prev*n1, res + '*' + str[0..i])
    r << tempOut if tempOut != ''
  end
  r
end

#puts strWayFinder('125',5)
#puts strWayFinder('120',2)
#puts strWayFinder('025',10)
puts strWayFinder('123',6)
#puts strWayFinder('121',0)
# fails for this
puts strWayFinder('123', 36)
=begin
123 , 6

12,  2

1, 1
dp(str,target,res)
for i in 0...str.length do
  in1 = str[0..i]
  curSum = in1.to_i
  resPlus = cursum + dp(str[i+1..-1]) if str[i+1..-1] != ''
  resMinus = cursum + str[i+1..-1] if str[i+1..-1] != ''


=end

def operator(str,target)
  #
end