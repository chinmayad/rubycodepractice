=begin
http://www.geeksforgeeks.org/finding-all-subsets-of-a-given-set-in-java/
=end

def subsets(arr)
  res = []
  for i in 0...(2**arr.size) do
    val = ''
    for j in 0...arr.size do
      val += arr[j] if (i & (1 << j)) != 0
    end
    res.push(val)
  end
  res
end

p subsets(['a','b','c','d'])