=begin
Print all nodes at distance k from a given node
Given a binary tree, a target node in the binary tree, and an integer value k, print all the nodes that are at distance k from the given target node. No parent pointers are available.

BinaryTree

Consider the tree shown in diagram

Input: target = pointer to node with data 8.
       root = pointer to node with data 20.
       k = 2.
Output : 10 14 22

If target is 14 and k is 3, then output
should be "4 20"

=end

class Node
  def initialize(val, left=nil, right=nil)
    @val = val
    @left = left
    @right = right
  end
  attr_accessor :val, :left, :right
end

def kthDistance(btree, target, k, depth = 0)
  return [], -1 if btree == nil
  res = []
  if btree == target
    res += nodesAtKDist(btree,k)
    return res, depth
  end

  leftRes, d = kthDistance(btree.left, target, k, depth+1)
  if d != -1
    res += leftRes
    relativeDepth = d - depth
    if k >= relativeDepth
      res += nodesAtKDist(btree.right, k-relativeDepth-1 )
    end
    return res, d
  end

  rightRes, d = kthDistance(btree.right, target, k, depth+1)
  if d != -1
    res += rightRes
    relativeDepth = d - depth
    if k >= relativeDepth
      res += nodesAtKDist(btree.left, k-relativeDepth)
    end
    return res, d
  end
end

def nodesAtKDist(root, k)
  return [] if k < 0 || root == nil
  return [root.val] if k == 0
  l = nodesAtKDist(root.left,k-1)
  r = nodesAtKDist(root.right,k-1)
  return l + r
end

btree = Node.new(20, x=Node.new(8, Node.new(4), Node.new(12, Node.new(10), Node.new(14))), Node.new(22))
p kthDistance(btree, x, 2)