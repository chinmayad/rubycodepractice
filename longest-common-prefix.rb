
# greatest common prefix between two strings in an array of strings
class Node
  def initialize(val, children={})
    @value = val
    @children = children
  end

  def insert_str(str)
    root = self
    str.each_char do |char|
      root = insert(root, char)
    end
    insert(root,'*')
  end

  def insert(root=self, char)
    return root.children[char] if root.children[char]
    n = Node.new(char)
    root.children[char] = n
    n
  end

  attr_accessor :value, :children
end

def makeDic(strs)
  return nil if strs == nil or strs.empty?
  root = Node.new('0')
  strs.each do |str|
    root.insert_str(str)
  end
  root
end

def findMaxDepth(d, depth = 0)
  maxDepth = 0
  maxDepth = depth if d.children.size > 1
  d.children.each do |char, child|
    treeDepth = findMaxDepth(child, depth+1)
    maxDepth = treeDepth unless maxDepth > treeDepth
  end
  return maxDepth
end

def prefix(strs)
  dic = makeDic(strs)
  return (findMaxDepth(dic))
end


s = ['abc', 'def', 'ghi', 'adb']
puts prefix(s)
s = ['leqt', 'letcode', 'leese', 'leybperv', 'lesevey']
puts prefix(s)
