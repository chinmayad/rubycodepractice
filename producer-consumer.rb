=begin
It was a simple producer-consumer with a queue of up to n items (multiple producers and consumers.) Something to that effect.
=end


class Buffer
  def initialize(size)
    @store = Array.new(size)
    @reader = 0
    @writer = 0
    @rLock = Mutex.new
    @wLock = Mutex.new
    @size = size
  end

  def write(data)
    while bufferFull do
      # nothing
    end
    @wLock.lock
    unless bufferFull
      @store[@writer] = data
      incrementWriter
    end
    @wLock.unlock
  end

  def bufferFull
    return true if (@reader == 0) && (@writer == @size - 1)
    return true if @writer == @reader - 1
    return false
  end

  def bufferEmpty
    return true if @reader == @writer
    return false
  end

  def read
    while bufferEmpty do
      # nothing
    end
    @rLock.lock
    unless bufferEmpty
      data = @store[@reader]
      incrementReader
    end
    @rLock.unlock
    data
  end

  def incrementReader
    @reader = (@reader + 1) % @size
  end

  def incrementWriter
    @writer = (@writer + 1) % @size
  end
end


b = Buffer.new(3)
b.write(4)
puts b.read
b.write(3)
b.write(4)
b.write(5)
b.write(3)
