#               .   O     〇      o     #=#  <- space station
#               →   ←     →       ←
#
# mass          1   5     7       3
# direction     1  -1     1      -1
#
# answer: 1

#               .   o   O   〇   #=#
#               →   →   →   ←
#
# mass          1   3   5   7
# direction     1   1   1  -1
#
# answer: 0

#               O   .   o   #=#
#               →   →   ←
#
# mass          5   1   4
# direction     1   1  -1
#
# answer: 1

#               .   O   o   #=#
#               →   →   ←
#
# mass          1   5   4
# direction     1   1  -1
#
# answer: 2


# c = 3
#
def count_hits(asteroids)
  return nil if asteroids == nil
  curSize = 0
  badAsteroids = 0
  # move from right to left
  asteroids.reverse.each do |a|
    if a.direction == 1 && a.mass > curSize
      curSize = 0
      badAsteroids += 1
    elsif a.direction == -1
      curSize = a.mass if curSize < a.mass
    else
      # do nothing
    end
  end
  return badAsteroids  # TODO
end

class Asteroid
  attr_reader :mass, :direction

  def initialize(mass, direction)
    @mass = mass
    @direction = direction
  end

  def to_s
    "Asteroid(#{@mass},#{@direction})"
  end
end

case1 = [
    Asteroid.new(1, 1),
    Asteroid.new(5, -1),
    Asteroid.new(7, 1),
    Asteroid.new(3, -1)
]

case2 = [
    Asteroid.new(1, 1),
    Asteroid.new(3, 1),
    Asteroid.new(5, 1),
    Asteroid.new(7, -1)
]

case3 = [
    Asteroid.new(5, 1),
    Asteroid.new(1, 1),
    Asteroid.new(4, -1)
]

case4 = [
    Asteroid.new(1, 1),
    Asteroid.new(5, 1),
    Asteroid.new(4, -1)
]


def test_case(case_num, asteroids, expected)
  actual = count_hits(asteroids)
  if expected == actual
    puts "Case #{case_num}: PASSED"
  else
    puts "Case #{case_num}: FAILED: expected #{expected} actual #{actual}"
  end
end


test_case(1, case1, 1)
test_case(2, case2, 0)
test_case(3, case3, 1)
test_case(4, case4, 2)