# solve a sudoku
class Sudoku
  $SIZE=9
  def initialize
    @p = gen_sudoku
    @s = @p.clone
    @tries = Array.new($SIZE){Array.new($SIZE) {|index| []}}
    @row_entries = []
    @col_entries = []
    @cube_entries = []
  end

  def gen_sudoku
   problem = [
              [5, 3, 0, 0, 7, 0, 0, 0, 0],
              [6, 0, 0, 1, 9, 5, 0, 0, 0],
              [0, 9, 8, 0, 0, 0, 0, 6, 0],
              [8, 0, 0, 0, 6, 0, 0, 0, 3],
              [4, 0, 0, 8, 0, 3, 0, 0, 1],
              [7, 0, 0 ,0, 2, 0, 0, 0, 6],
              [0, 0, 0, 4, 1, 9, 0, 0, 5],
              [0, 6, 0, 0, 0, 0, 2, 8, 0],
              [0, 0, 0, 0, 8, 0, 0, 7, 9]
             ]
  end

  def possible_row_entries
    for i in 0...$SIZE do
      sample_set = [1,2,3,4,5,6,7,8,9]
      for j in 0...$SIZE do
        next if @p[i][j] == 0
        sample_set.delete(@p[i][j])
      end
      @row_entries[i] = sample_set.clone
    end
  end

  def possible_col_entries
    for i in 0...$SIZE do
      sample_set = [1,2,3,4,5,6,7,8,9]
      for j in 0...$SIZE do
        next if @p[j][i] == 0
        sample_set.delete(@p[j][i])
      end
      @col_entries[i] = sample_set.clone
    end
  end

  def possible_cube_entries
    for k in 0...$SIZE
      sample_set = [1,2,3,4,5,6,7,8,9]
      for i in 0...$SIZE/3 do
        for j in 0...$SIZE/3 do
          next if @p[(k % 3)*3 + i][(k/3)*3 + j] == 0
          sample_set.delete(@p[(k % 3)*3 + i][(k/3)*3 + j])
        end
      end
      @cube_entries[k] = sample_set.clone
    end
  end

  def solve_sudoku
    possible_row_entries # 1, 2, 4, 6, 8, 9
    possible_col_entries
    possible_cube_entries

    if run_solve(0, 0)
      return @s
    else
      return nil
    end
  end

  def intersection(i, j, element)
  #  try = intersection(i, j, row_e[i], col_e[j], cube_e[(i/3)*3 + (j/3)], nil)
    element = nil if element == 0
    row_e = @row_entries[i]
    col_e = @col_entries[j]
    cube_e = @cube_entries[(i/3)*3 + (j/3)]
    @tries[i][j].push(element) unless (element == nil or @tries[i][j].include?(element))
    row_e.each do |e|
      next if ( @tries[i][j].include?(e) )
      if (col_e.include?(e) and cube_e.include?(e))
        row_e.delete(e) && col_e.delete(e) && cube_e.delete(e)
        (row_e.push(element) && col_e.push(element) && cube_e.push(element)) unless element == nil
        return e
      end
    end
    (row_e.push(element) && col_e.push(element) && cube_e.push(element)) unless element == nil
    return false
  end

  def next_square(i, j)
    j += 1
    if j == $SIZE
      i += 1
      j = 0
    end
    return i, j
  end

  def run_solve(i, j) # cube = (i/3)*3 + (j/3)
    if @p[i][j] != 0
      i,j = next_square(i, j)
      return run_solve(i,j)
    end

    try = intersection(i, j, @s[i][j])
    if try == false
      @s[i][j] = 0
      return false
    end
    @s[i][j] = try
    try = nil
    new_i, new_j = next_square(i, j)
    return true if ((new_i >= $SIZE) or (new_j >= $SIZE))
    while run_solve(new_i, new_j) == false do
      try = intersection(i, j, @s[i][j])
      if try == false
        @s[i][j] = 0
        return false
      end
      @s[i][j] = try
    end
    true
  end
end


s = Sudoku.new
p s.solve_sudoku