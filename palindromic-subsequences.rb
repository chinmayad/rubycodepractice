=begin
Find how many palindromic subsequence (need not necessarily be distinct) can be formed in a given string. Note that the empty string is not considered as a palindrome.
Examples:

Input : str = "abcd"
Output : 4
Explanation :- palindromic  subsequence are : "a" ,"b", "c" ,"d"

Input : str = "aab"
Output : 4
Explanation :- palindromic subsequence are :"a", "a", "b", "aa"

Input : str = "aaaa"
Output : 15

Input : str = 'aba'
Output: 'a','b','a','aa', 'aba'   (Subsequences are different from substrings!, hence the 'aa')
=end

# aba
def pCount(str)
  cache = {}
  pSubs(str,cache)
end

def pSubs(str, cache)
  return cache[str] if cache[str]
  return 0 if str == '' || str == nil
  i = 0
  j = str.size - 1
  return 1 if i == j

  if str[i] == str[j]
    res =  1 + pSubs(str[1..-1], cache) + pSubs(str[0..j-1], cache) #- pSubs(str[1..j-1], cache)
    # the "- psubs(str[1..j-1])" is absent here because the equation is actually
    # 1 + pSubs(str[1..-1], cache) + pSubs(str[0..j-1], cache) - pSubs(str[1..j-1], cache) + pSubs(str[1..j-1], cache)
    # the third term is there because all palindromes between i+1 and j -1 are counted twice.
    # The fourth term is there because every palindrome 'p' formed between the two will have another palindrome with
    # it of the form s[i] + 'p' + s[j] because s[i] == s[j] in this block.
  else
    res = pSubs(str[1..-1], cache) + pSubs(str[0..j-1],cache)  - pSubs(str[1..j-1],cache)
    # here s[i] != s[j] so there is no 4th term. Hence the negative third term only.
  end
  cache[str] = res
  return res
end

puts pCount('abcd')
puts pCount('aba')



# [a,b,c]
['',a,b,c,ab,ac,bc,abc]


