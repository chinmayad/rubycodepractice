=begin
Given an array of float prices, round each price such that the total sum of the prices stays as close as possible
 to the previous sum but the prices in the array are converted to round numbers IE (36.46 4.54) will become (36, 5)
input : array of decimals ~ X
output : array of int ~ Y
But they need to satisfy the condition:
sum(Y) = round(sum(x))
minmize (|y1-x1| + |y2-x2| + ... + |yn-xn|)
Example1:
input = 30.3, 2.4, 3.5
output = 30 2 4

Example2:
input = 30.9, 2.4, 3.9
output = 31 2 4
=end

def rounder(prices)
  # sort prices by decimal value (descending)
  # calculate sum of prices in float and round (sum1)
  # floor every num in prices and calculate sum (sum2)
  # increment each num in prices by 1 upto index = sum2-sum1
  prices.sort_by! {|x| x - x.floor}
  prices.reverse!
  floatSum = 0
  integerSum = 0
  prices.each_with_index do |p, index|
    floatSum += p
    prices[index] = p.floor
    integerSum += p.floor
  end

  tolerance = floatSum.round - integerSum
  for i in 0...tolerance do
    prices[i] += 1
  end
  prices
end




p rounder([30.9, 2.4, 3.9])
p rounder([30.3, 2.4, 3.5])