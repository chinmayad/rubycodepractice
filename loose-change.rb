=begin
given array of coins (floats) hit the target sum. how many ways.
=end


def changer(coins,target)
  coins.sort!
  changeMaker(coins, target)
end

def changeMaker(coins, target)
  res = []
  coins.each do |c|
    return res if c.to_f > target.to_f
    return [[c]] if c.to_f == target.to_f
    temp = changeMaker(coins, target-c)
    temp.each do |r|
      r.push(c)
    end
    res += temp
  end
  return res
end


coins = [3.5, 1.5, 2]
target = 9
p changer(coins, target)