=begin
https://leetcode.com/problems/max-points-on-a-line/description/
Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.
=end


#Definition for a point.
class Point
  attr_accessor :x, :y
  def initialize(x=0, y=0)
    @x = x
    @y = y
  end
end
# @param {Point[]} points
# @return {Integer}
def max_points(points)
  lines = {}
  return 1 if points.size == 1
  max = 0
  points.each_with_index do |p, index|
    for i in index+1...points.length do
      m,c = getLine(p, points[i])
      if lines[[m,c]]
        lines[[m,c]].push(index) if not lines[[m,c]].include?(index)
        lines[[m,c]].push(i) if not lines[[m,c]].include?(i)
      else
        lines[[m,c]] = []
        lines[[m,c]].push(index)
        lines[[m,c]].push(i)
      end
    end
  end
  lines.each do |line, ps|
    if ps.size > max
      max = ps.size
    end
  end
  max
end

def getLine(p1,p2)
  num = p2.y - p1.y
  denum = p2.x - p1.x
  if denum == 0
    if p1.x == 0
      return 10000, 10001
    end
    return 10000, 10000
  end
  m = denum.to_f/num.to_f
  c = p1.y - m * p1.x
  return m,c
end


p = [Point.new(1,2), Point.new(2,3), Point.new(3,4), Point.new(0,0)]
p = [Point.new(4,0),Point.new(4,-1),Point.new(4,5)]
x =[[0,-12],[5,2],[2,5],[0,-5],[1,5],[2,-2],[5,-4],[3,4],[-2,4],[-1,4],[0,-5],[0,-8],[-2,-1],[0,-11],[0,-9]]
p2 = []
x.each do |p|
  p2.push(Point.new(p[0], p[1]))
end
puts max_points(p2)