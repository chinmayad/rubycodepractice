=begin
Given two words word1 and word2, find the minimum number of steps required to convert word1 to word2. (each operation is counted as 1 step.)

You have the following 3 operations permitted on a word:

a) Insert a character
b) Delete a character
c) Replace a character
=end

# @param {String} word1
# @param {String} word2
# @return {Integer}
def min_distance(w1, w2)
  return w1.size if w2.empty?
  return w2.size if w1.empty?
  return min_distance(w1[1..-1], w2[1..-1]) if w1[0] == w2[0]
  return 1 + min( min_distance(w1[1..-1], w2), min_distance(w1, w2[1..-1]), min_distance(w1[1..-1], w2[1..-1]) )
end

def min(a,b,c)
  min = a < b ? a : b
  min < c ? min : c
end


p min_distance('geeks', 'geseks')
p min_distance('quoys', 'bladder')
p min_distance('coo', 'woos')