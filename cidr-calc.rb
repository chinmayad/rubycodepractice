=begin
Given a string ip and number n, print all cidr addresses that cover that range
- This is one of the problems that is out of left field.

Given an IPv4 IP address p and an integer n,
return a list of CIDR strings that most succinctly
represents the range of IP addresses from p to (p + n).

Ex :
192.168.1.2 , n = 1024
Ending ip = 192.168.5.2
tightest cidr = 192.168.1.2/21

Ex :
192.168.1.2 , n = 4
Ending ip = 192.168.1.6
Ans : 192.168.1.6/32

cidr = /29
=end

def ipToNum(ip)
  ips = ip.split('.')
  res = 0
  ips.reverse.each_with_index do |val,i|
    res += (val.to_i << (8 * i))
  end
  res
end

def numToIp(n)
  res = []
  for i in 0..3 do
    res.push(n & 0x0FF)
    n = n >> 8
  end
  res.reverse.join('.')
end

def ipToCidr(start,n)
  return nil if start == nil or start.count('.') != 3
  startN = ipToNum(start)
  res = []
  endN = startN + n
  while (startN <= endN) do
    firstOne = startN & (-startN)
    first1thBit = Math.log2(firstOne).floor
    numBitsForDiff = Math.log2(endN - startN + 1).floor
    minBit = first1thBit < numBitsForDiff ? first1thBit : numBitsForDiff
    sIp = numToIp(startN)
    res.push(sIp + '/' + (32-minBit).to_s)
    startN += (1 << minBit)
  end
  res

  # n = findmatch(ip, lastIp) # n is num bits from right that are common in two ips
  # from lsb of start, move from 0 to 32-n bits, for each bit
  #    if start(i) == 0 && end(i) == 0
  #       next
  #    else
  #       res.push()
end

puts ipToCidr('192.168.1.2', 4)