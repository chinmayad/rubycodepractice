=begin
Given a set of candidate numbers (C) (without duplicates) and a target number (T), find all unique combinations in C where the candidate numbers sums to T.

The same repeated number may be chosen from C unlimited number of times.

Note:
All numbers (including target) will be positive integers.
The solution set must not contain duplicate combinations.
For example, given candidate set [2, 3, 6, 7] and target 7,
A solution set is:
[
  [7],
  [2, 2, 3]
]
=end

def combination_sum(c, target)
  c.sort!
  return [] if (c == nil or c.empty?)
  solve_bucket(c, target)
end

def solve_bucket(c, target)
  return [] if (c == nil or c.empty?)
  if c[0] > target
    return []
  end
  if c[0] == target
    return [[target]]
  end
  entry = c[0]
  set_of_ans = []
  repetitions = target/entry
  set_of_ans.push(Array.new(repetitions, entry)) if repetitions * entry == target
  while repetitions >= 0
    soln_set = Array.new(repetitions, entry)

    arr_of_ans = solve_bucket(c - [entry], target-(entry * repetitions))
    arr_of_ans.each do |arr|
      arr.concat(soln_set)
    end
    repetitions -= 1
    set_of_ans.concat(arr_of_ans)
  end
  set_of_ans
end


p combination_sum([1,2], 2)
p combination_sum([2,3,6,7], 15)