# implement a queue using 2 stacks

require_relative 'minofstack'
module Test
  class Queue
    def initialize
      @s1 = Stack.new()
      @s2 = Stack.new()
    end

    def enqueue(v)
      @s1.push(v)
    end

    def dequeue
      updatestacks if @s2.size == 0
      return nil if @s2.size == 0
      @s2.pop
    end

    def updatestacks
      len = @s1.size
      while len > 0 do
        @s2.push(@s1.pop)
        len -= 1
      end
    end
    private :updatestacks
  end
end

q = Test::Queue.new()
q.enqueue(5)
q.enqueue(7)
q.enqueue(9)
q.enqueue(11)
puts q.dequeue
puts q.dequeue
puts q.dequeue
puts q.dequeue
puts q.dequeue
q.enqueue(13)
puts q.dequeue