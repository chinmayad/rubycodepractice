=begin
TinyURL is a URL shortening service where you enter a URL such as https://leetcode.com/problems/design-tinyurl and it returns a short URL such as http://tinyurl.com/4e9iAk.

Design the encode and decode methods for the TinyURL service. There is no restriction on how your encode/decode algorithm should work.
You just need to ensure that a URL can be encoded to a tiny URL and the tiny URL can be decoded to the original URL.

=end

class URLShortener
  def initialize
    @tinyUrlHash = {}
    @longUrlHash = {}
    @lastEncodedURL = '0000000'
  end

  def invalid(url)
    true if url == nil
    false
  end

  def getHash(index=0)
    i = @lastEncodedURL.size - 1 - index
    case @lastEncodedURL[i]
      when 'z'
        @lastEncodedURL[i] = 'A'
      when 'Z'
        @lastEncodedURL[i] = '0'
        getHash(index+1)
      when '9'
        @lastEncodedURL[i] = 'a'
      else
        @lastEncodedURL[i] = @lastEncodedURL[i].next
    end
    @lastEncodedURL
  end

  def encode(longUrl)
    return nil if invalid(longUrl)
    if tinyUrl = @tinyUrlHash[longUrl]
      return 'http://tinyurl.com/' + tinyUrl
    end
    key = getHash
    @tinyUrlHash[longUrl] = key
    @longUrlHash[key] = longUrl
    'http://tinyurl.com/' + key
  end

  # Decodes a shortened URL to its original URL.
  #
  # @param {string} shortUrl
  # @return {string}
  def decode(shortUrl)
    key = urlParse(shortUrl)
    @longUrlHash[key]
  end

  def urlParse(url)
    url.gsub('http://tinyurl.com/', '')
  end

end

u = URLShortener.new
for i in 0..250 do
puts u.encode("https://leetcode-#{i}.com")
end

puts u.decode('http://tinyurl.com/0000019')


