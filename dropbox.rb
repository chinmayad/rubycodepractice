def say_hello
  puts 'Hello, World'
end

5.times { say_hello }


# 2d grid
# each cell can be alive or dead
# 8 neighbors
# 1 alive, 0 dead

# - If the cell is alive and if it has 2 or 3 alive neighbors, it stays alive. Otherwise, it dies.
# - If the cell is dead and if it has exactly 3 alive neighbors, it becomes alive. Otherwise, it stays dead.
# - The state of each cell is determined only by the state of its neighbors in the last iteration.

# return grid with state changes
def nextState(grid)
  return grid if grid == nil
  length = grid.size
  breadth = grid[0].size
  newGrid = Array.new(length) {Array.new(breadth) {0}}
  grid.each do |row|
    row.each do |col|
      newGrid[row][col] = getNextState(grid,row, col, length, breadth)
    end
  end
  newGrid
end
# [[0,1,0],
#  [1,0,0],
#  [1,1,1]]
def getNextState(grid, r, c, len, bre)
  case grid[r][c]
    when 1 # alive
    neighborCount = getAliveNeighbors(grid, r, c, len, bre)
    if neighborCount == 2 || neighborCount == 3
      return 1
    else
      return 0
    end
    when 0 # dead
    neighborCount = getAliveNeighbors(grid, r, c, len, bre)
    if neighborCount == 3
      return 1
    else
      return 0
    end
  end
  raise(Exception, 'unknown state of grid')
end


def getAliveNeighbors(grid, r, c, len, bre)
  neighbors = 0
  neighbors += 1 if ( ((r + 1) < len) && ((c + 1) < bre) && (grid[r+1][c+1] == 1) )
  neighbors += 1 if ( ((c + 1) < bre)                    && (grid[r][c+1] == 1) )
  neighbors += 1 if ( ((r + 1) < len) && ((c - 1) >= 0)  && (grid[r+1][c-1] == 1) )
  neighbors += 1 if ( ((r - 1) >= 0)  && ((c + 1) < bre) && (grid[r-1][c+1] == 1) )
  neighbors += 1 if ( ((r - 1) >= 0)                     && (grid[r-1][c] == 1) )
  neighbors += 1 if ( ((r - 1) >= 0)  &&  ((c - 1) >= 0) && (grid[r-1][c-1] == 1) )
  neighbors += 1 if ( ((c - 1) >= 0)                     && (grid[r][c-1] == 1) )
  neighbors += 1 if ( ((r + 1) < len)                    && (grid[r+1][c] == 1) )
  return neighbors
end
