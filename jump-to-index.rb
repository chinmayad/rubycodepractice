=begin
Given an array of non-negative integers, you are initially positioned at the first index of the array.

Each element in the array represents your maximum jump length at that position.

Your goal is to reach the last index in the minimum number of jumps.

For example:
Given array A = [2,3,1,1,4]

The minimum number of jumps to reach the last index is 2. (Jump 1 step from index 0 to 1, then 3 steps to the last index.)
=end

def jump(nums)
  len = (nums.length)
  soln = Array.new(len) {65535}
  soln[0] = 0
  nums.each_with_index do |range, index|
    return soln[index] if (index == (len - 1))
    range.times do |distance|
      distance += 1
      break if (index+distance) >= len
      soln[index+distance] = (soln[index]+1) if soln[index]+1 < soln[index+distance]
      return soln[index+distance] if index+distance == len-1
    end
  end
end

def max(a,b)
  a > b ? a : b
end

def jump2(nums)
  farthest_index = 0
  curEnd = 0
  hop = 0
  len = nums.length
  index = 0
  while index < (len - 1)
    range = nums[index]
    farthest_index = max(farthest_index, index+range)
    if index == curEnd
      hop += 1
      curEnd = farthest_index
    end
    index += 1
  end
  return hop
end


def jump3(nums)
  farthest_index = 0
  endpoint = 0
  index = 0
  hop = 0
  while index < nums.length do
    farthest_index = max(farthest_index, index + nums[index])
    if index == endpoint
      hop += 1
      endpoint = farthest_index
    end
    index += 1
  end
  return hop
end

puts jump3([1,2,3,4,5,6])
puts jump3([2,3,1,1,4])
puts jump3([1,1,1,1,1,1,1,1])
puts jump3([2,2,1,1,2,1,2,3,1,1])
puts jump3([2,4,1,5,1,1,1,1,4,1])
puts jump3((1..25000).to_a.reverse)
