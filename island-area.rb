=begin
Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

Find the maximum area of an island in the given 2D array. (If there is no island, the maximum area is 0.)

Example 1:
[[0,0,1,0,0,0,0,1,0,0,0,0,0],
 [0,0,0,0,0,0,0,1,1,1,0,0,0],
 [0,1,1,0,1,0,0,0,0,0,0,0,0],
 [0,1,0,0,1,1,0,0,1,0,1,0,0],
 [0,1,0,0,1,1,0,0,1,1,1,0,0],
 [0,0,0,0,0,0,0,0,0,0,1,0,0],
 [0,0,0,0,0,0,0,1,1,1,0,0,0],
 [0,0,0,0,0,0,0,1,1,0,0,0,0]]
Given the above grid, return 6. Note the answer is not 11, because the island must be connected 4-directionally.
Example 2:
[[0,0,0,0,0,0,0,0]]
Given the above grid, return 0.
Note: The length of each dimension in the given grid does not exceed 50.
=end

def num_distinct_islands(grid)
  graph_set = {}
  grid.each_with_index do |row, r|
    row.each_with_index do |entry, c|
      next if entry == 0
      graph = []
      dfs(grid, r, c, graph)
      offset = offset_graph(graph)
      compare_and_store(offset, graph_set)
    end
  end
  distinct_set(graph_set)
end

def distinct_set(g)
  g.size
end

def offset_graph(g)
  base = g[0].clone
  for i in 0...g.size do
    g[i][0] -= base[0]
    g[i][1] -= base[1]
  end
  g
end

def compare_and_store(g, set)
  set.each_key do |graph|
    if graph_compare(graph,g)
      set[graph] += 1
      return
    end
  end
  set[g] = 1
  return
end

def graph_compare(g1,g2)
  for i in 0...g1.size do
    return false if i >= g2.size
    return false if g1[i] != g2[i]
  end
  return true if i == g2.size - 1
  return false
end

def dfs(grid, r, c, graph)
  area = 1
  graph.push([r,c])
  neighbors(grid, r,c).each do |coord|
    area += dfs(grid, coord[0], coord[1], graph)
  end
  return area
end


def neighbors(grid, r, c)
  rows = grid.size
  cols = grid[0].size
  grid[r][c] = 0
  res = []
  res.push([r+1,c]) && grid[r+1][c] = 0 if (r+1 < rows && grid[r+1][c] == 1)
  res.push([r,c+1]) && grid[r][c+1] = 0 if (c+1 < cols && grid[r][c+1] == 1)
  res.push([r-1,c]) && grid[r-1][c] = 0 if (r-1 >= 0   && grid[r-1][c] == 1)
  res.push([r,c-1]) && grid[r][c-1] = 0 if (c-1 >= 0   && grid[r][c-1] == 1)
  res
end

g = [[0,0,1,0,0,0,0,1,0,0,0,0,0],
     [0,0,1,0,0,0,0,1,1,1,0,0,0],
     [0,1,1,0,1,0,0,0,0,0,0,0,0],
     [0,1,0,0,1,1,0,0,1,0,1,0,0],
     [0,1,0,0,1,1,0,0,1,1,1,0,0],
     [0,0,0,0,0,0,0,0,0,0,1,0,0],
     [0,0,0,0,0,0,0,1,1,1,1,0,0],
     [0,0,0,0,0,0,0,1,1,0,0,0,0]]

q = [[0,1,0],
     [1,1,0],
     [1,1,0],]
r = [[0,0,0,0,0,0,0,0]]
s = [[1,1,0,1,1],
     [1,0,0,0,0],
     [0,0,0,0,1],
     [1,1,0,1,1]]
t = [[1,1,0,0,0],
     [1,1,0,0,0],
     [0,0,0,1,1],
     [0,0,0,1,1]]

u = [[1,1,0,1,1],
     [1,0,0,0,0],
     [0,0,0,0,1],
     [1,1,0,1,1]]

#puts max_area_of_island(g)
#puts max_area_of_island(q)
puts num_distinct_islands(u)
puts num_distinct_islands(t)