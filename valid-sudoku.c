/* # The Sudoku board could be partially filled, where empty cells are filled with the character '.'.
   # A valid Sudoku board (partially filled) is not necessarily solvable. Only the filled cells need to be validated.
*/

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

bool checkRows(char** board, int rows) {
  int cols = rows;
  for (int i = 0 ; i < rows; i++) {
     char* hash = (char*) malloc(rows * sizeof(char));
     for (int j = 0; j < cols; j++) {
        char val = board[i][j];
        if (hash[val-1] == 1) {
          return false;
        } else {
          hash[val-1] = 1;
        }
     }
  }
  return true;
}

bool checkColumns(char** board, int cols) {
  int rows = cols;
  for (int i = 0 ; i < cols; i++) {
     char* hash = (char*) malloc(cols * sizeof(char));
     for (int j = 0; j < rows; j++) {
        char val = board[j][i];
        if (hash[val-1] == 1) {
          return false;
        } else {
          hash[val-1] = 1;
        }
     }
  }
  return true;
}

bool checkCubes(char** board, int num_cubes) {
  for (int i = 0; i < num_cubes; i ++) {
    bool soln = checkRows(&board[(i/3)*3][(i*3)%9], num_cubes/3) && checkColumns(&board[(i/3)*3][(i*3)/3], num_cubes/3);
    if (soln == false) {
      return false;
    }
  }
  return true;
}



bool isValidSudoku(char** board, int boardRowSize, int boardColSize) {
  return checkRows(board, boardRowSize) && checkColumns(board, boardColSize) && checkCubes(board, boardColSize);
}

char** constructBoard(size) {
  char** board = (char**)malloc(size*size*sizeof(char));
  for (int i = 1; i <= size; i++) {
    for (int j = 1; j <=size; j++) {
      int value = (i * j) % 10;
      // value = (value == 0) ? 0: value;
      board[i-1][j-1] = (char)value;
    }
  }
  return board;
}

int main() {
  const int SIZE = 9;
  char** board = constructBoard(SIZE);
  if (isValidSudoku(board, SIZE, SIZE)) {
    printf("%s", "true!!");
  }
}



