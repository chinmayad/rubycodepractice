=begin
Given a 2D board and a list of words from the dictionary, find all words in the board.

Each word must be constructed from letters of sequentially adjacent cell, where "adjacent" cells are those horizontally or vertically neighboring. The same letter cell may not be used more than once in a word.

For example,
Given words = ["oath","pea","eat","rain"] and board =

[
  ['o','a','a','n'],
  ['e','t','a','e'],
  ['i','h','k','r'],
  ['i','f','l','v']
]
Return ["eat","oath"].
Note:
You may assume that all inputs are consist of lowercase letters a-z.
=end

def find_words(board, words)
  dimension = board.size
  res = []
  trie = Trie.new(words)
  for i in 0...dimension do
    for j in 0...dimension do
      out = dfs(trie, board, i, j)
      res += out if out
    end
  end
  return res
end

def dfs(trie, board, r, c)
  trie.clear_counter
  return dfs_search(trie, board, r, c)
end

def dfs_search(trie, board, r, c)
  word = trie.word_contains(board[r][c])
  return false if word == nil
  res = []
  res.push('') if word == true
  neighbors(board, r, c).each do |coord|
    retval = dfs_search(trie, board, coord[0], coord[1])
    res += retval if retval
    return append(board[r][c],res) if res
  end
  return res
end

def append(c, arr)
  arr.each do |a|
    a.unshift(c)
  end
  return arr
end

def neighbors(board, r, c)
  dimension = board.size
  res = []
  res.push([r+1, c]) if r + 1 < dimension
  res.push([r, c+1]) if c + 1 < dimension
  res.push([r-1, c]) if r - 1 >= 0
  res.push([r, c-1]) if c + 1 >= 0
  res
end

class Trie
  def initialize(words)
    @root = Node.new('0')
    @tracker = @root
    create_trie(words)
  end

  def create_trie(words)
    words.each do |w|
      insert_str(w)
    end
  end

  def insert_str(w)
    parent = @root
    w.each_char do |c|
      parent = insert_char(parent, c)
    end
    insert_char(parent, '*')
  end

  def insert_char(parent, c)
    if parent.children[c]
      return parent.children[c]
    else
      n = Node.new(c)
      parent.children[c] = n
      return n
    end
  end

  def clear_counter
    @tracker = @root
  end

  def word_contains(c)
    return nil if not @tracker.children[c]
    @tracker = @tracker.children[c]
    return true if @tracker.children['*']
    return false
  end
end

class Node
  def initialize(val, children = {})
    @val = val
    @children = children
  end
  attr_accessor :val, :children
end

board = [
    ['o','a','a','n'],
    ['e','t','a','e'],
    ['i','h','k','r'],
    ['i','f','l','v']
]
words = ["oath","pea","eat","rain", "oathfi"]

p find_words(board, words)
