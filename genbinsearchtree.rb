# Given a sorted array with unique integer elements, create a bin search tree with min height

class KNode
  def initialize(val, left=nil, right=nil)
    @value = val
    @left = left
    @right = right
  end

  attr_accessor :value, :left, :right
end


def genbinsearchtree(arr)
  #validateinput(arr)
  max = arr.size - 1
  min = 0
  maketree(arr, min, max)
end

def maketree(arr, min, max)
  return nil if max < min
  m = getmidpoint(min, max)
  root = KNode.new(arr[m])
  root.left  = maketree(arr, min, m - 1)
  root.right = maketree(arr, m + 1, max )
  root
end

def getmidpoint(min, max)
  return (min + max) / 2
end

# Tests
a = [1,3,6,7,8, 13]
root = genbinsearchtree(a)
puts 'complted test'


######################
#  1,3 ,6 ,7, 8
#       6
#    1      8
# 3      7     9
#                  15
#               13     17
