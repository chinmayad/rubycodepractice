=begin
The Stable Marriage Problem states that given N men and N women, where each person has ranked all members of the opposite sex in order of preference, marry the men and women together such that there are no two people of opposite sex who would both rather have each other than their current partners. If there are no such people, all the marriages are “stable” (Source Wiki).

Consider the following example.

Let there be two men m1 and m2 and two women w1 and w2.
Let m1‘s list of preferences be {w1, w2}
Let m2‘s list of preferences be {w1, w2}
Let w1‘s list of preferences be {m1, m2}
Let w2‘s list of preferences be {m1, m2}

The matching { {m1, w2}, {w1, m2} } is not stable because m1 and w1 would prefer each other over their assigned partners. The matching {m1, w1} and {m2, w2} is stable because there are no two people of opposite sex that would prefer each other over their assigned partners.

=end

def prefers(arr, one, two)
  arr.each do |a|
    if a == one
      return true
    elsif a == two
      return false
    end
  end
end

def propose(m,w,pairsM,pairsW, free)
  ex = pairsW[w]
  pairsM[m] = w
  pairsW[w] = m
  free.push(ex) if ex
end

def stableMarriage(men,women)
  pairsM = {}
  pairsW = {}
  free = (1..men.size).to_a
  while not free.empty?
    mId = free.pop
    m = men[mId-1]
    m.each do |w|
      if ((enemy = pairsW[w]) && prefers(women[w-1], enemy, mId))
        # move on
      else
        propose(mId,w, pairsM, pairsW, free)
        break;
      end
    end
  end
  pairsM
end

men = [[1,2,3],[2,3,1],[2,1,3]]
women = [[3,2,1],[1,3,2],[1,2,3]]
puts stableMarriage(men,women)