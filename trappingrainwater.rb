=begin
Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.

For example,
Given [0,1,0,2,1,0,1,3,2,1,2,1], return 6.
https://leetcode.com/problems/trapping-rain-water/description/
=end


def trap(height)
  return 0 if height == nil or height.size == 0
  begPtr = 0
  sum = 0
  while begPtr < height.size && height[begPtr] == 0
    begPtr += 1
  end
  endPtr = begPtr + 1
  while endPtr < height.size
    endVal = height[endPtr]
    if endVal >= height[begPtr]
      sum += calculate(height, begPtr, endPtr)
      begPtr = endPtr
    end
    endPtr += 1
  end
  if height[endPtr-1] && (height[endPtr-1] < height[begPtr])
    sum += trap(height[begPtr...endPtr].reverse)
  end
  return sum
end

def calculate(h, start, finish)
  return 0 if start == finish
  sum = 0
  for i in (start+1)...finish
    sum += h[start] - h[i]
  end
  return sum
end

puts trap([0,1,0,2,1,0,1,3,2,1,2,1])
puts trap([0])