require_relative 'stack'

class SortedStack
  def initialize
    @s1 = Stack.new
    @temp = Stack.new
  end

  def push(val)
    while ( @s1.size != 0 && val > @s1.peek)
      @temp.push(@s1.pop)
    end
    @s1.push(val)
    while (@temp.size != 0)
      @s1.push(@temp.pop)
    end
  end

  def pop
    @s1.pop
  end
end

sq = SortedStack.new
sq.push(7)
sq.push(6)
sq.push(12)
sq.push(44)
puts sq.pop
puts sq.pop
puts sq.pop
puts sq.pop