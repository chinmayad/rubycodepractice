=begin
Given a string (no spaces) and a pattern, see if they match.

"xyzxy" and "airbnbairbn" return true.
"ccc" and "airairair" return true.
=end


=begin
x => 0, 3
y => 1, 4
z => 2


a => 0 , 6
i => 1,  7
r => 2,  8
b => 3,  9
n => 4,  10
b => 5
=end

def patterner(str,pattern, plen=1)
  pmatch = {}
  j = 0
  str.each_char do |c|
    if pmatch[c]
      return false if plen+j > pattern.size
      if pmatch[c] == pattern[j...plen+j]
        j+=plen
      else
       plenmax = pattern.size/str.size
       return false if plen == plenmax
       return patterner(str,pattern,plen+1)
      end
    else
      return false if plen+j > pattern.size
      pmatch[c] = pattern[j...plen+j]
      j += plen
    end
  end
  return true
end

str = 'xyzxy'
p = 'airbnbairbn'
puts patterner(str,p)

str = 'ccc'
p = 'airairair'
puts patterner(str,p)

str = 'xyzxy'
p = 'airbnbairbn'
puts patterner(str,p)