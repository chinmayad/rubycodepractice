require_relative 'slinkedlist'
class Levels
  def initialize(level)
    @level = level
    @list = SinglyLinkedList.new
  end
end

class ParkingSpot
  @@spot_num = 0
  CARCLASSES = {'MOTORCYCLE' => 1, 'COMPACT' => 2, 'LARGE' => 3}
  def initialize
    @id = self.class.gen_id
    @available = true   # will be false if maintenance is being performed on the spot for example
    @occupied = false
    @level = @id/10

  end

  def self.gen_id
    @@spot_num += 1
  end

  def available?
    @available
  end

  def occupied?
    @occupied
  end

  def occupied
    @occupied = true
  end

  def vacant
    @occupied = false
  end

  def get_id
    @id
  end

  def get_level
    @level
  end

  def self.get_num_spots
    @@spot_num
  end

  def status
    puts 'id: ' + self.get_id.to_s
    puts 'dimensions: ' + self.get_dimensions
    puts 'available?: ' + self.available?.to_s
    puts 'occupied?: ' +  self.occupied?.to_s
    puts 'level: ' +  self.get_level.to_s
    puts ' *************************************** '
  end
end


class CompactSpot < ParkingSpot
  @@LENGTH = '3 metres'
  @@WIDTH  = '2 metres'
  def initialize
    super
  end

  def get_dimensions
    return @@LENGTH + ' x ' +  @@WIDTH
  end

  def allowed_vehicles
    CARCLASSES.map {|key,val| key if val <= CARCLASSES['COMPACT']}
  end
end

class LargeSpot < ParkingSpot
  @@LENGTH = '3 metres'
  @@WIDTH  = '2 metres'
  def initialize
    super
  end

  def get_dimensions
    return @@LENGTH + ' x ' +  @@WIDTH
  end

  def allowed_vehicles
    CARCLASSES.map {|key,val| key if val <= CARCLASSES['LARGE']}
  end
end

class MotorcycleSpot < ParkingSpot
  @@LENGTH = '1 metre'
  @@WIDTH  = '0.5 metre'
  def initialize
    super
  end

  def get_dimensions
    return @@LENGTH + ' x ' +  @@WIDTH
  end

  def allowed_vehicles
    CARCLASSES.map {|key,val| key if val <= CARCLASSES['MOTORCYCLE']}
  end
end

c1 = CompactSpot.new
l1 = LargeSpot.new
m1 = MotorcycleSpot.new

c2 = CompactSpot.new
l2 = LargeSpot.new
m2 = MotorcycleSpot.new

c1.status
c2.status
l1.status
l2.status
m1.status
m2.status

puts c1.allowed_vehicles
puts m1.allowed_vehicles
puts l2.allowed_vehicles

