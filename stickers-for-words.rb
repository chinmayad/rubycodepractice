=begin
stickers = ["jello", "hollow", "yes"]
pattern = "yellow"
ans = 2 (need two stickers to construct pattern.) Minimum. 3 is wrong
=end

def min_stickers(stickers, pattern)
  memo = {}
  comp = {}
  store = {}
  m = stickers_for_words(memo,comp, store, stickers, pattern,1000, 0)
  return -1 if m == 1000
  m
end

def stickers_for_words(memo, comp, store, stickers, pattern, current_min, depth)
  h = (depth.to_s + current_min.to_s + pattern).hash
  ret = memo[h]
  return ret if ret
  min = 1000

  if depth >= current_min
    memo[h] = min
    return min
  end
  for i in 0...stickers.size do
    s = stickers[i]
    rem = get_max_match(store, s, pattern) unless rem = comp[s+pattern]
    comp[s+pattern] = rem
    if rem == pattern
      next
    end
    if rem == ''
      memo[h] = 1
      return 1
    end
    current_min = min if min < current_min
    temp = 1 + stickers_for_words(memo, comp, store, stickers, rem, current_min, depth+1)
    min = temp if temp < min
  end
  memo[h] = min
  return min
end

def get_hash(store, s)
  hashs = {}
  if store[s]
    hashs = store[s]
  else
    s.each_char do |e|
      if hashs[e]
        hashs[e] += 1
      else
        hashs[e] = 1
      end
    end
    store[s] = hashs
  end
  hashs.dup
end

def get_max_match(store,s,p)
  hashs = get_hash(store,s)
  res = ''
  p.each_char do |e|
    if (hashs[e]) && hashs[e] > 0
      hashs[e] -= 1
    else
      res += e
    end
  end
  return res
end
t1 = Time.now
s = ["hello", "world"]
p = 'how'
puts min_stickers(s,p)

s = ["jello", "hollow", "yes"]
p= "yellow"
puts min_stickers(s,p)

s = ["reclu", "rocquve", "losy"]
p= "reclosy"
puts min_stickers(s,p)

s = ["with","example","science"]
p = "thehat"
puts min_stickers(s,p)

s = ["travel","quotient","nose","wrote","any"]
p = "lastwest"
puts min_stickers(s,p)

s = ["these","guess","about","garden","him"]
p = "atomher"
puts min_stickers(s,p)

s = ["dad","rose","pay","else","condition","were","east","nor"]
p = "easesoldier"
puts min_stickers(s,p)


s = ["summer","sky","cent","bright","kill","forest","neighbor","capital","tall"]
p = 'originalchair'
puts min_stickers(s,p)

s = ["feed","industry","let","pair","milk","hope"]
p = "likehuman"
puts min_stickers(s,p)


s = ["this","island","keep","spring","problem","subject"]
p = 'gasproper'
puts min_stickers(s,p)

s = ["heavy","claim","seven","set","had","it","dead","jump","design","question","sugar","dress","any","special","ground","huge","use","busy","prove","there","lone","window","trip","also","hot","choose","tie","several","be","that","corn","after","excite","insect","cat","cook","glad","like","wont","gray","especially","level","when","cover","ocean","try","clean","property","root","wing"]
p = "travelbell"
puts min_stickers(s,p)

s = ["sheet","whether","always","keep","distant","fun","thick","similar","best"]
p = "liftwife"
puts min_stickers(s,p)

s = ["heavy","claim","period","son","brought","as","street","slip","pass","dear","lie","flower","support","sky","tiny","add","much","call","change","smell","body","begin","knew","triangle","see","syllable","symbol","safe","gas","free","quite","blood","broke","half","sing","month","those","enemy","stone","shop","oh","life","quiet","face","try","seat","near","continue","root","bone"]
p = "solveside"
puts min_stickers(s,p)

s = ["love","color","dollar","son","feet","held","star","still","children","wonder","sell","city","law","eye","loud","prepare","opposite","except","should","music","tell","white","several","roll","then","we","oil","led","huge","act","but","coast","warm","five","atom","world","hand","cool","lost","these","plain","solution","bird","leave","turn","pick","mouth","voice","root","original"]
p = "feltmillion"

puts min_stickers(s,p)
puts Time.now - t1