=begin
Given an integer n, return 1 - n in lexicographical order.

For example, given 13, return: [1,10,11,12,13,2,3,4,5,6,7,8,9].

[1,10...19,100, 101..109,1]
[1,10,100,1000-1009, 10]

Please optimize your algorithm to use less time and space. The input size may be as large as 5,000,000.
=end

#
#


def lex(n)
  res = []
  for i in 1..n
    res.push(i.to_s)
  end
  res.sort!
  res2 = []
  res.each do |e|
    res2.push(e.to_i) if e.to_i <= n
  end
  res2
end

# puts lex(1234)

# start with 1 '' '' '' (n digits)

class LexCounter
  def initialize()
    @res = []
  end

  def lex2(n, k = 1)

    for i in 0..9 do
      return if k > n
      @res.push(k)
      k = k * 10
      lex2(n,k)
      k = k/10
      k = k + 1
      return if k % 10 == 0
    end
  end

  def res
    @res
  end
end


lc = LexCounter.new
lc.lex2(110)
puts lc.res

