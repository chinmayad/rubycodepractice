# given two strings, and 3 types of action (insert a character, delete or replace a char) find if the two strings are
# 1 or 0 edits away

def oneaway(s1, s2)
  return true if s1 == s2
  return false if ((s1.length - s2.length).abs > 1)
  sreplace = 0
  sdelete = 0
  if s1.length > s2.length
    sdelete  = check_for_delete(s1, s2)
  elsif s1.length < s2.length
    sdelete  = check_for_delete(s2, s1)
  else
    sreplace = check_for_replace(s1, s2)
  end
  puts "replace status #{sreplace}"
  puts "delete status #{sdelete}"
end

def check_for_replace(s1, s2)
  cnt = 0
  while(cnt < s1.length) do
    if s1[cnt] != s2[cnt]
      v = s1.slice(cnt+1, -1)
      q = s2.slice(cnt+1, -1)
      return true if s1.slice(cnt+1, s1.length) == s2.slice(cnt+1, s2.length)
      return false
    end
    cnt += 1
  end
  true
end

def check_for_delete(s1, s2)
  cnt = 0
  while (cnt < s2.length)
    if s1[cnt] != s2[cnt]
      return true if s1.slice(cnt+1, s1.length) == s2.slice(cnt, s2.length)
      return false
    end
    cnt += 1
  end
  true
end



oneaway("hey", "heay")
oneaway("pale", "ple")
oneaway("pale", "bale")
oneaway("pale", "bake")
oneaway("pale", "paker")