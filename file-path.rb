=begin
implement a function normalizePath() that takes DOS style path e.g."/hello/world/here/../I/come" and
normalizes it by removing path elements equal to ".." and their parent elements
=end

# Runtime O(3n)
def normalizePath(path)
  stack = Array.new
  dirs = path.split('/')   # O(n)
  dirs.each do |dir|       # O(n)
    next if dir == ''      # ignore doubles slashes
    if dir == '..'         # pop a dir from stack
      raise "invalid path #{path}" if not stack.pop  # if no elements left in stack, the path has more .. than dirs.
      next
    end
    stack.push(dir)
  end
  if path[0] == '/'                # O(n)
    return '/' + stack.join('/')   # append / at the beginning for absolute paths
  else
    stack.join('/')                # relative paths
  end
end

puts normalizePath('/abc/def//ghi/../../q')
puts normalizePath('/abc/def//ghi/../../../q')
puts normalizePath('abc/def//ghi/../..')
puts normalizePath('/abc/def//ghi/hello world/cc/dd/../../q')