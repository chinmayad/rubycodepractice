=begin
Given an unsorted array of nonnegative integers, find a continous subarray which adds to a given number.
Input: arr[] = {1, 4, 20, 3, 10, 5}, sum = 33
Ouptut: Sum found between indexes 2 and 4

Input: arr[] = {1, 4, 0, 0, 3, 10, 5}, sum = 7
Ouptut: Sum found between indexes 1 and 4

Input: arr[] = {1, 4}, sum = 0
Output: No subarray found
=end

# 1 => findcsum(4,20,3,10,5) 32
# 4 => findcsum([20,3,10,5], 29), findcsum([20,3,10,5],28) , findcsum([20,3,10,5],33)runningsum = 5
# 20 => findcsum([3,10,5],8) (target-runningsum+currentnum), findcsum([3,10,5],33) (excluding 20), findcsum([3,10,5],
# 13) (including
# 20)


def summer(arr,sum, fsum = sum, res=[])
  return res if sum == 0
  return false if arr.empty? || sum < 0
  e = arr[0]
  ret = summer(arr-[e], sum-e, fsum, res + [e])
  return ret if ret
  ret = summer(arr-[e], fsum, fsum, [])
  return ret if ret
  false
end

p summer([1, 4, 20, 3, 10, 5], 33)
p summer([1, 4, 0, 0, 3, 10, 5], 7)
p summer([1, 4], 0)