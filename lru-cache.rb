# design a lru cache
class Node
  def initialize(key,val,pre=nil,nex=nil)
    @val = val
    @key = key
    @pre = pre
    @nex = nex
  end
  attr_accessor :val, :nex, :pre, :key
end

class DoublyLinkedList
  def initialize(size)
    @capacity = size
    @size = 0
    @head = nil
  end

  def insert(key, value)
    delete(@head.pre) if self.size == @capacity
    if @head == nil
      @head = Node.new(key,value)
      @head.pre = @head
      @head.nex = @head
      @size += 1
      return @head
    end
    temp = @head
    @size += 1
    @head = Node.new(key,value, temp.pre, temp)
  end

  def delete(node)
    if node == @head
      @head = node.nex
    end
    node.pre.nex = node.nex
    node.nex.pre = node.pre
    @size -= 1
  end

  def size
    @size
  end

  def evict
    temp = @head.pre
    delete(@head.pre)
    temp.key
  end
end

class Cache
  def initialize(size)
    @maxSize = size
    @store = Hash.new()
    @dll = DoublyLinkedList.new(size)
  end

  def write(key,val)
    if @store[key]
      @dll.delete(@store[key])
      @store[key] = @dll.insert(key,val)
    else
      evictFromCache if @store.size == @maxSize
      @store[key] = @dll.insert(key,val)
    end
  end

  def read(key)
    return false if not @store[key]
    value = @store[key].val
    @dll.delete(@store[key])
    @store[key] = @dll.insert(key,value)
    return value
  end

  def evictFromCache
    key = @dll.evict
    @store.delete(key)

  end

  def listSize
    return @store.size, @dll.size
  end
end

cache = Cache.new(5)
p cache.write('name1', 'dexter')
p cache.write('name2', 'fuzzums')
p cache.write('name3', 'fuzzums')
p cache.write('name4', 'fuzzums')
p cache.read('name2')
p cache.write('name6', 'deedee')
p cache.read('name7')
p cache.read('name1')
p cache.write('name9', 'deedee1')
p cache.write('name8', 'deedee2')
p cache.write('name0', 'deedee3')
p cache.write('name10', 'deedee4')
p cache.write('name11', 'deedee5')
