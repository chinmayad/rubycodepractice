# 3 stacks, move disks from stack 1 to stack 3.  cant have large disc on small one
require_relative 'stack'

class Hanoi
  def initialize(num_disks)
    @num_disks = num_disks
    @s1 = Stack.new
    @s2 = Stack.new
    @s3 = Stack.new
    disk_id = num_disks - 1
    while (disk_id >= 0 ) do
      @s1.push(disk_id)
      disk_id = disk_id - 1
    end
  end

  def moveHanoi(height=@s1.size, src=@s1,dest=@s2,buffer=@s3)
    if height == 1
      dest.push(src.pop)
    else
      moveHanoi(height-1, src, buffer, dest)
      dest.push(src.pop)
      moveHanoi(height-1, buffer, dest, src)
    end
  end
end

h = Hanoi.new(6)
h.moveHanoi
puts 'hi'