# a child can go up stairs in steps of 1 2 or 3. Find number of different ways it can climb n stairs

def stepcount(n)
  traverse(n)
end


def cachevar
  @cachevar = [0] unless @cachevar
  @cachevar
end

def traverse(x)
  return 1 if x == 0
  return cachevar[x] if cachevar[x]
  a,b,c = 0,0,0
  a = traverse(x-1)
  cachevar[x-1] = a
  b = traverse(x-2) unless x < 2
  c = traverse(x-3) unless x < 3
  return (a + b + c)
end

def traverseslow(x)
  return 1 if x == 0
  a,b,c = 0,0,0
  a = traverseslow(x-1)
  b = traverseslow(x-2) unless x < 2
  c = traverseslow(x-3) unless x < 3
  return (a + b + c)
end


btime = Time.now
puts traverse(25)
puts Time.now - btime

btime = Time.now
puts traverseslow(25)
puts Time.now - btime
