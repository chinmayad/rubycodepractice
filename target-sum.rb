# @param {Integer[]} nums
# @param {Integer} target
# @return {Integer[]}
def two_sum(nums, target)
  return nil if nums == nil
  return nil if nums.size < 2
  remainder = {}
  index = 0
  nums.each do |num|
    if remainder.include?(target-num)
      return [remainder[target-num], index]
    else
      remainder[num] = index
    end
    index += 1
  end
  return false
end


arr = [1,3,5,7,9,11,15]
puts two_sum(arr, 17)
puts two_sum(nil, 11)
print two_sum(arr, 14)


# 1, 3, 5, 7, 15, 17
# 2, 4, 6, 12, 13

# 1 3 5 7
# 9 11 15 17 19


