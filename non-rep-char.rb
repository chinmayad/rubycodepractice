=begin
Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return -1.

Examples:

s = "leetcode"
return 0.

s = "loveleetcode",
return 2.
Note: You may assume the string contain only lowercase letters.
=end

def first_uniq_char(s)
  return nil if s == nil
  hash = {}
  check_str(hash, s)
end

def check_str(hash, s)
  for i in 0...s.size do
    c = s[i]
    if hash[c]
      hash[c] = 100000
    else
      hash[c] = i
    end
  end

  h = hash.sort_by {|k,v| v}
  return -1 if not h.first or h.first[1] == 100000
  h.first[1]
end

puts first_uniq_char('leetcode')
puts first_uniq_char('loveleetcode')

