=begin
implement a hashmap
=end

class Entry
  def initialize(key,value, nex = nil)
    @key = key
    @value = value
    @nex = nex
  end
  attr_accessor :key, :value, :nex
end

class LinkedList
  def initialize
    @head = nil
    @size = 0
  end

  # inserts element if it doesnt exist and returns size
  def insert(e)
    iter = @head
    if iter == nil
      @head = e
      @size += 1
      return 1
    end
    while iter.nex != nil && iter.key != e.key do
      iter = iter.nex
    end
    if iter.key == e.key
      iter.value = e.value
      return 0
    end
    iter.nex = e
    @size += 1
    return 1
  end

  def find(key)
    iter = @head
    while iter != nil
      return iter.value if iter.key == key
      iter = iter.nex
    end
    nil
  end
end

class HashMap
  def initialize(capacity, loadFactor)
    @capacity = capacity
    @loadFactor = loadFactor
    @table = Array.new(capacity) {nil}
    @numEntries = 0
  end

  def getHash(key)
    key.object_id  # TO DO
  end

  def set(key,value)
    hash = getHash(key)
    e = Entry.new(key,value)
    store(e,hash)
  end

  def rehash
    # TODO
  end

  def store(e,hash)
    index = hash % @capacity
    if @table[index] == nil
      entryList = LinkedList.new
      @table[index] = entryList
    else
      entryList = @table[index]
    end
    @numEntries += entryList.insert(e)
    if @numEntries > @capacity.to_f * @loadFactor
      rehash
    end
  end

  def get(key)
    hash = getHash(key)
    index = hash % @capacity
    if @table[index] == nil
      return nil
    else
      entryList = @table[index]
    end
    entryList.find(key)
  end

  private :store, :rehash, :getHash
end

h = HashMap.new(16, 0.75)
puts h.set(1, 'one1')
puts h.set(3, 'three')
puts h.set(5, 'five5')
puts h.set(1, 'one')
puts h.set(3, 'three')
puts h.set(5, 'five')
puts h.set(6, 'one1')
puts h.set(4, 'three')
puts h.set(2, 'five5')
puts h.set(15, 'one')
puts h.set(33, 'three')
puts h.set(2, 'five')
puts h.get(1)