=begin
http://blog.gainlo.co/index.php/2016/09/12/dropbox-interview-design-hit-counter/
=end


class HitCounter
  def initialize
    @q = Array.new
    @m = Mutex.new
    Thread.new do
      while true do
        if !@q.empty?
          @m.lock
          if (@q[@q.size-1] < (Time.now - (1 * 30)))
            @q.pop
          end
          @m.unlock
        else
          sleep 1
        end
      end
    end
  end


  def logHits
    t = Time.now
    @m.lock
    @q.unshift(t)
    @m.unlock
  end

  def getHits
    @q.size
  end
end

h = HitCounter.new
100.times do
  h.logHits
end
puts h.getHits
sleep 20
puts h.getHits
sleep 8
h.logHits
puts h.getHits
sleep 11
puts h.getHits
h.logHits
h.logHits
sleep 1
puts h.getHits
sleep 4
puts h.getHits
sleep 1
puts h.getHits