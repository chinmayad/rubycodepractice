=begin
Given a tree where every right node is a leaf, meaning that  it can only extend on the left side,
invert it such that the leftmost node is now the root,
 and all of the previous left nodes are now on the right side. The former right nodes will be on the left side.
=end

def flipTree(root)
  return root if root == nil || root.left == nil
  newRoot = flipTree(root.left)
  newRoot.left = newRoot.right
  newRoot.right = root
  root.left = root.right
  root.right = nil
  return newRoot
end


class Node
  def initialize(val, left=nil, right=nil)
    @val = val
    @left = left
    @right = right
  end
  attr_accessor :val, :left, :right
end

b = Node.new(1,Node.new(3, Node.new(5, Node.new(7,nil,Node.new(6)),Node.new(4)),Node.new(2)),Node.new(0))
x = flipTree(b)
puts 'hi'

#          1
#       3     0
#    5     2
# 7    4
#   6


#      7
#   3     1
#2      0
#