# Complete the function below.

def doesCircleExist(commands)
  result = []
  commands.each_with_index do |c,index|
    next if index == 0
    loc1, dir1 = executeAndTest(c, [0,0], [0,1])
    if loc1 == [0,0]
      result[index-1] = 'YES'
      next
    end
    if dir1 == [0,1]
      result[index-1] = 'NO'
    else
      result[index-1] = 'YES'
    end
  end
  result
end

def executeAndTest(sequence, start, dir)
  curLocation = start
  direction = dir
  sequence.each_char do |c|
    case c
      when 'G'
        curLocation[0] += direction[0]
        curLocation[1] += direction[1]
      when 'R'
        case direction
          when [1,0]
            direction = [0,-1]
          when [0,-1]
            direction = [-1,0]
          when [-1,0]
            direction = [0,1]
          when [0,1]
            direction = [1,0]
        end
      when 'L'
        case direction
          when [1,0]
            direction = [0,1]
          when [0,1]
            direction = [-1,0]
          when [-1,0]
            direction = [0,-1]
          when [0,-1]
            direction = [1,0]
        end
    end
  end
  return curLocation, direction
end

p doesCircleExist([4,'G', 'GRGL', 'LLLL', 'R'])
        