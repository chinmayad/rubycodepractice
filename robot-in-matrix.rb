def numberOfPaths(m)
  cache = {}
  cache[[m.size-1,m[0].size-1]] = 1
  traceMatrix(m, 0, 0, cache)
end

def traceMatrix(m,r,c, cache)
  return cache[[r,c]] if cache[[r,c]]
  if m[r][c] == 0
    cache[[r,c]] = 0
    return 0
  end
  if r == m.size-1
    ways = traceMatrix(m,r,c+1,cache)
  elsif c == m[0].size-1
    ways = traceMatrix(m,r+1,c, cache)
  else
    ways = traceMatrix(m,r+1,c,cache) + traceMatrix(m,r,c+1, cache)
  end
  cache[[r,c]] = ways
  return ways
end

puts numberOfPaths([[1,1],[1,1]])