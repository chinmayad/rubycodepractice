# Run the dfs search on a graph and find a node
module DFS
  def find(node)
    return nil if node == nil
    depth = findNode(node)
    self.clearvisited
    depth
  end


  def findNode(node, depth = 0)
    return nil if self.visited?
    self.visited
    return depth if self == node
    self.children.each do |child|
      depthval = child.findNode(node, depth + 1)
      return depthval if depthval != nil
    end
    return nil
  end
end

class Node
  include DFS
  @@initcount = 0

  def self.initcount
    @@initcount
  end

  def self.initcount=(val)
    @@initcount = val
  end

  def initialize(val, children = [])
    @val = val
    @children = children
    @visited = self.class.initcount
  end

  def addEdge(node)
    self.children.push(node)
  end

  def addNodeWithEdge(val)
    newNode = Node.new(val)
    self.children.push(newNode)
    newNode
  end

  def visited?
    return true if @visited != self.class.initcount
    return false
  end

  def visited
    @visited = @visited + 1
  end

  def clearvisited
    self.class.initcount = @visited
  end

  attr_accessor :val, :children
end



  root = Node.new(1)
  n2 = root.addNodeWithEdge(5)
  n3 = n2.addNodeWithEdge(13)
  n4 = n3.addNodeWithEdge(11)
  n5 = n4.addNodeWithEdge(6)
  n5.addEdge(n2)
  n6 = n5.addNodeWithEdge(42)
  n3.addNodeWithEdge(19)
  n7 = n5.addNodeWithEdge(29)
  n8 = n7.addNodeWithEdge(33)
  n12 = n8.addNodeWithEdge(43)
  n12.addEdge(n8)
  n10 = Node.new(99)
  n11 = Node.new(14)
  n2.addEdge(n11)
  n11.addEdge(root)

puts root.find(n12)
puts root.find(n2)
#     1 - > 5 -> 13 -> 11 -> 6 -> 42      99
#     <-14 <-       -> 19      -> 29 -> 33 <-> 43

